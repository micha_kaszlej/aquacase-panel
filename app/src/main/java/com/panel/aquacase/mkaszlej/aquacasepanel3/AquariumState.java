package com.panel.aquacase.mkaszlej.aquacasepanel3;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AquariumState {

    public enum ActivityType {
        MANUAL(0),
        AUTO(1),
        LOCKSCREEN(2),
        ANIMATIONSELECT(3),
        CALENDARADD(4),
        CALENDAR(5),
        COLOR(6),
        DISPLAY(7),
        LIGHT(8),
        PIN(9),
        RGB(10),
        SCHEDULE(11),
        SIDE(12),
        THERMO(13),
        TIMESELECT(14),
        WEB(15),
        FEED(16),
        O2(17),
        CO2(18),
        LPUMP(19),
        RPUMP(20),
        VOLCANO(21),
        AQUARIUMMANUAL(22),
        PANELMANUAL(23),
        GUARANTEEMANUAL(24),
        FACTORYRESET(25),
        CODE(26),
        NO_CONNECTION(27),
        ANIMATIONSHOW(28),
        CONNECT(29);

        public int value;

        ActivityType(int value){
            this.value = value;
        }

        public static ActivityType getStateFromValue(int state_value) {
            for (ActivityType s : ActivityType.values()) {
                if (s.value == state_value) return s;
            }
            throw new IllegalArgumentException("ActivityType not found.");
        }
    }

    ArrayList<CalendarEvent> events;
    ArrayList<WorkCycle> workCycles;

    String xmlOrigin;

    public int userId;
    public int userCode;
    public int messageId;

    public int workcycleId;
    public int calendarEventId;

    public long currentTime;

    public int currentScreenId;
    public int currentScreenType;

    public int mode;

    public int pinCode;

    public int leftPumpActive;
    public int rightPumpActive;
    public int lightActive;
    public int rgbActive;
    public int outRgbActive;
    public int o2Active;
    public int volcanoActive;
    public int co2Active;
    public int foodActive;

    public int lightPower;

    public int currentTemperature;
    public int desiredTemperature;

    public int rgbColorId;
    public int rgbPower;
    public int rgbAnimationType;

    public int outRgbColor;
    public int outRgbPower;
    public int outRgbAnimationType;

    public int bgResourceId;

    public int dispTimeOff;
    public int dispTimeLock;

    public int languageCode;

    //TODO: Calendar Sync
    //TODO: Schedule times
    public AquariumState()
    {
        xmlOrigin = "";

        userId = 0;
        userCode = 0;
        messageId = 0;

        workcycleId = 0;
        calendarEventId = 0;

        currentTime = System.currentTimeMillis();

        mode = -1;

        currentScreenId = -1;

        pinCode = -1;

        leftPumpActive = -1;
        rightPumpActive = -1;
        lightActive = -1;
        rgbActive = -1;
        outRgbActive = -1;
        o2Active = -1;
        volcanoActive = -1;
        co2Active = -1;
        foodActive = -1;
        currentTemperature = -1;
        desiredTemperature = -1;

        lightPower = -1;

        rgbAnimationType = -1;
        rgbPower = -1;

        outRgbAnimationType = -1;
        outRgbPower = -1;

        rgbColorId = -1;
        outRgbColor = -1;

        languageCode = -1;

        bgResourceId = -1;

        dispTimeOff = -1;
        dispTimeLock = -1;

        events = new ArrayList<CalendarEvent>();
        workCycles = new ArrayList<WorkCycle>();

    }


    public void increaseMessageId() {
        messageId++;
    }

    public void increaseWorkcycleId() {
        workcycleId++;
    }

    public void increaseCalendarEventId() {
        calendarEventId++;
    }

    public String toString(){
        return
            "{"+
                    "\"userId\":"+userId+","+
                    "\"messageId\":"+messageId+","+
                    "\"userCode\":"+userCode+","+
                    "\"isAquarium\":"+0+","+
                    "\"currentTime\":"+System.currentTimeMillis()+","+
                    "\"currentScreenId\":"+currentScreenId+","+
                    "\"currentScreenType\":"+currentScreenType+","+
                    "\"mode\":"+mode+","+
                    "\"pinCode\":"+pinCode+","+
                    "\"lpump\":"+leftPumpActive+","+
                    "\"rpump\":"+rightPumpActive+","+
                    "\"light\":"+lightActive+","+
                    "\"rgb\":"+rgbActive+","+
                    "\"outRgb\":"+outRgbActive+","+
                    "\"o2\":"+o2Active+","+
                    "\"volcano\":"+volcanoActive+","+
                    "\"co2\":"+co2Active+","+
                    "\"food\":"+foodActive+","+
                    "\"lightPower\":"+lightPower+","+
                    "\"currentT\":"+currentTemperature+","+
                    "\"desiredT\":"+desiredTemperature+","+
                    "\"RGBColor\":"+ rgbColorId +","+
                    "\"RGBPower\":"+rgbPower+","+
                    "\"RGBAnim\":"+rgbAnimationType+","+
                    "\"outRGBColor\":"+outRgbColor+","+
                    "\"outRGBPower\":"+outRgbPower+","+
                    "\"outRGBAnim\":"+outRgbAnimationType+","+
                    "\"bg\":"+bgResourceId+","+
                    "\"dispOff\":"+dispTimeOff+","+
                    "\"dispLock\":"+dispTimeLock+","+
                    "\"lang\":"+languageCode+","+
                    "\"calendarEventId\":"+calendarEventId+","+
                    "\"workcycleId\":"+workcycleId+""+
            "}";
    }

    public ArrayList<NameValuePair> serialize()
    {
        ArrayList<NameValuePair> l = new ArrayList<NameValuePair>();
        l.add(new BasicNameValuePair("userId", Integer.toString(userId)));
        l.add(new BasicNameValuePair("messageId", Integer.toString(messageId)));
        l.add(new BasicNameValuePair("userCode", Integer.toString(userCode)));
        l.add(new BasicNameValuePair("isAquarium", Integer.toString(0)));
        l.add(new BasicNameValuePair("currentTime", Long.toString(System.currentTimeMillis())));
        l.add(new BasicNameValuePair("currentScreenId", Integer.toString(currentScreenId)));
        l.add(new BasicNameValuePair("currentScreenType", Integer.toString(currentScreenType)));
        l.add(new BasicNameValuePair("mode", Integer.toString(mode)));
        l.add(new BasicNameValuePair("pinCode", Integer.toString(pinCode)));
        l.add(new BasicNameValuePair("lpump", Integer.toString(leftPumpActive)));
        l.add(new BasicNameValuePair("rpump", Integer.toString(rightPumpActive)));
        l.add(new BasicNameValuePair("light", Integer.toString(lightActive)));
        l.add(new BasicNameValuePair("rgb", Integer.toString(rgbActive)));
        l.add(new BasicNameValuePair("outRgb", Integer.toString(outRgbActive)));
        l.add(new BasicNameValuePair("o2", Integer.toString(o2Active)));
        l.add(new BasicNameValuePair("volcano", Integer.toString(volcanoActive)));
        l.add(new BasicNameValuePair("co2", Integer.toString(co2Active)));
        l.add(new BasicNameValuePair("food", Integer.toString(foodActive)));
        l.add(new BasicNameValuePair("lightPower", Integer.toString(lightPower)));
        l.add(new BasicNameValuePair("currentT", Integer.toString(currentTemperature)));
        l.add(new BasicNameValuePair("desiredT", Integer.toString(desiredTemperature)));
        l.add(new BasicNameValuePair("RGBColor", Integer.toString(rgbColorId)));
        l.add(new BasicNameValuePair("RGBPower", Integer.toString(rgbPower)));
        l.add(new BasicNameValuePair("RGBAnim", Integer.toString(rgbAnimationType)));
        l.add(new BasicNameValuePair("outRGBColor", Integer.toString(outRgbColor)));
        l.add(new BasicNameValuePair("outRGBPower", Integer.toString(outRgbPower)));
        l.add(new BasicNameValuePair("outRGBAnim", Integer.toString(outRgbAnimationType)));
        l.add(new BasicNameValuePair("bg", Integer.toString(bgResourceId)));
        l.add(new BasicNameValuePair("dispOff", Integer.toString(dispTimeOff)));
        l.add(new BasicNameValuePair("dispLock", Integer.toString(dispTimeLock)));
        l.add(new BasicNameValuePair("lang", Integer.toString(languageCode)));
        l.add(new BasicNameValuePair("workcycleId", Integer.toString(workcycleId)));
        l.add(new BasicNameValuePair("calendarEventId", Integer.toString(calendarEventId)));

        return l;
    }

    private String parseCalendarEvents()
    {
        if(events.size() == 0) return "";
        String ret = "";
        for(CalendarEvent event : events){
            ret += event.toJSON() +",";
        }
        return  stripLastComa(ret);
    }

    private String parseWorkCycles()
    {
        if(workCycles.size() == 0) return "";
        String ret = "";
        for(WorkCycle cycle : workCycles)
        {
            ret += cycle.toJSON() +",";
        }
        stripLastComa(ret);
        return  stripLastComa(ret);
    }

    public String stripLastComa(String str)
    {
        if (str.length() > 0 && str.charAt(str.length()-1)==',')
        {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    public void update(JSONObject status)
    {
        try
        {
            userId = status.getInt("userId");
            messageId = status.getInt("messageId");
            userCode = status.getInt("userCode");
            currentTime = status.getLong("currentTime");
            mode = status.getInt("mode");
            currentScreenId = status.getInt("currentScreenId");
            currentScreenType = status.getInt("currentScreenType");
            pinCode = status.getInt("pinCode");
            leftPumpActive = status.getInt("leftPumpActive");
            rightPumpActive = status.getInt("rightPumpActive");
            lightActive = status.getInt("lightActive");
            rgbActive = status.getInt("rgbActive");
            outRgbActive = status.getInt("outRgbActive");
            o2Active = status.getInt("o2Active");
            volcanoActive = status.getInt("volcanoActive");
            co2Active = status.getInt("co2Active");
            foodActive = status.getInt("foodActive");
            lightPower = status.getInt("lightPower");
            currentTemperature = status.getInt("currentTemperature");
            desiredTemperature = status.getInt("desiredTemperature");
            rgbAnimationType = status.getInt("rgbAnimationType");
            rgbPower = status.getInt("rgbPower");
            outRgbAnimationType = status.getInt("outRgbAnimationType");
            outRgbPower = status.getInt("outRgbPower");
            rgbColorId = status.getInt("rgbColor");
            outRgbColor = status.getInt("outRgbColor");
            languageCode = status.getInt("languageCode");
            bgResourceId = status.getInt("bgResourceId");
            dispTimeOff = status.getInt("dispTimeOff");
            dispTimeLock = status.getInt("dispTimeLock");
            workcycleId = status.getInt("workcycleId");
            calendarEventId = status.getInt("calendarEventId");
        }
        catch(JSONException e){

        }
    }

    public void updateCycles(JSONArray cycles)
    {
        try
        {
            workCycles.clear();
            for (int i = 0; i < cycles.length(); i++)
            {
                // get first product object from JSON Array
                JSONObject cycle = cycles.getJSONObject(i);

                int type = cycle.getInt("type");
                int idInType = cycle.getInt("idInType");
                int hStart = cycle.getInt("hStart");
                int mStart = cycle.getInt("mStart");
                int hEnd = cycle.getInt("hEnd");
                int mEnd = cycle.getInt("mEnd");
                int isSet = cycle.getInt("isSet");

                workCycles.add( new WorkCycle( type , idInType , hStart , mStart , hEnd , mEnd , isSet ) );
            }
        }
        catch(JSONException e){}
    }


    public void updateCalendarEvents(JSONArray cal_events)
    {
        try
        {
            events.clear();
            for (int i = 0; i < cal_events.length(); i++)
            {
                // get first product object from JSON Array
                JSONObject event = cal_events.getJSONObject(i);

                float timestamp = System.currentTimeMillis();
                int num = event.getInt("num");
                int task_id = event.getInt("taskId");
                boolean delete_task = event.getInt("deleteTask") > 0;
                boolean is_aquarium = event.getInt("isAquarium") > 0;
                boolean aqua_ack = event.getInt("aquaAck") > 0;
                boolean mobile_ack = event.getInt("mobileAck") > 0;
                boolean alarm = event.getInt("alarm") > 0;
                String date_string = event.getString("date");
                String time_string = event.getString("time");
                String txt = event.getString("txt");

                events.add( new CalendarEvent(timestamp, num, task_id, delete_task, is_aquarium, aqua_ack, mobile_ack, alarm, date_string, time_string, txt) );
            }
        }
        catch(JSONException e){}
    }


}