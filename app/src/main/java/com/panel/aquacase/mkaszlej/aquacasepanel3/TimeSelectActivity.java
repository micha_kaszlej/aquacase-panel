package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

public class TimeSelectActivity extends AquaBaseActivity {

    int activityType;
    int startHour, startMinute, endHour, endMinute , workCycleId, isSet;
    int currentWorkCycleRid;

    TimePicker t1;
    TimePicker t2;
    TimePicker t3;
    TextView startTimeLabel;
    TextView endTimeLabel;
    TextView workCycleCaption;
    TextView resultTime;
    NumberPicker n;
    Button accept;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.TIMESELECT.value;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_select);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityType = extras.getInt("TYPE");
            startHour =  extras.getInt("STARTH");
            startMinute =  extras.getInt("STARTM");
            endHour =  extras.getInt("ENDH");
            endMinute =  extras.getInt("ENDM");
            workCycleId = extras.getInt("CYCLEID",0);
            isSet = extras.getInt("ISSET",0);
            currentWorkCycleRid = extras.getInt("WORKCYCLE", R.string.firstWorkCycle);
        }
        else
        {
            activityType = AquariumState.ActivityType.LIGHT.value;
        }

        initializeView();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    private void initializeView()
    {
        startTimeLabel = findViewById(R.id.startTimeLabel);
        endTimeLabel = findViewById(R.id.endTimeLabel);
        workCycleCaption = findViewById(R.id.workCycleCaption);
        resultTime = findViewById(R.id.resultTime);

        t1 = findViewById(R.id.timePicker);
        t2 = findViewById(R.id.timePicker2);
        n = findViewById(R.id.numberPicker);

        accept = findViewById(R.id.acceptButton);

        t1.setIs24HourView(true);
        t1.setOnTimeChangedListener(
                new TimePicker.OnTimeChangedListener() {

                    @Override
                    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                        startHour = hourOfDay;
                        startMinute = minute;
                        updateResultTime();
                    }
                }
        );

        t2.setIs24HourView(true);
        t2.setOnTimeChangedListener(
                new TimePicker.OnTimeChangedListener() {

                    @Override
                    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                        endHour = hourOfDay;
                        endMinute = minute;
                        updateResultTime();
                    }
                }
        );
    }

    @Override
    public void recreateFromState()
    {
        t1.setCurrentHour(startHour);
        t1.setCurrentMinute(startMinute);
        t2.setCurrentHour(endHour);
        t2.setCurrentMinute(endMinute);

        //AUTO FEEDER
        if( activityType == AquariumState.ActivityType.FEED.value ){
            endTimeLabel.setVisibility(View.GONE);
            startTimeLabel.setText(R.string.feedTime);
            t2.setVisibility(View.GONE);
        }
        else  if( activityType == AquariumState.ActivityType.CO2.value ) {
            n.setMaxValue(90);
            n.setMinValue(0);

            n.setOnValueChangedListener(
                    new NumberPicker.OnValueChangeListener() {

                        @Override
                        public void onValueChange(NumberPicker view, int value, int minute) {
                            startMinute = minute;
                            updateResultTime();
                        }
                    }
            );

            endTimeLabel.setVisibility(View.GONE);
            startTimeLabel.setText(R.string.workTime);
            n.setVisibility(View.VISIBLE);
            t1.setVisibility(View.GONE);
            t2.setVisibility(View.GONE);
        }

        if(activityType == AquariumState.ActivityType.CO2.value  )
        {
            workCycleCaption.setText(R.string.workTime);
        }
        else
        {
            workCycleCaption.setText(getResources().getString(currentWorkCycleRid));
        }
    }

    private void updateResultTime()
    {
        //AUTO FEEDER
        if( activityType == AquariumState.ActivityType.FEED.value ){
            resultTime.setText(startHour + ":" + ((startMinute > 9) ? startMinute : "0" + startMinute)) ;
        }
        else if(  activityType == AquariumState.ActivityType.CO2.value  ){
            resultTime.setText( ( ( startMinute > 9 ) ? startMinute : "0"+startMinute ) + " min" ) ;
        }
        else{
            resultTime.setText(startHour + ":" + ( ( startMinute > 9 ) ? startMinute : "0"+startMinute ) + " - " + endHour + ":" + ( ( endMinute > 9 ) ? endMinute : "0"+endMinute ) );
            if (endHour < startHour || ((endHour == startHour) && (endMinute <= startMinute))) {
                resultTime.setTextColor(getResources().getColor(R.color.red));
                accept.setVisibility(View.INVISIBLE);
            } else {
                resultTime.setTextColor(getResources().getColor(R.color.caldroid_white));
                accept.setVisibility(View.VISIBLE);
            }
        }
    }

    public void acceptAction( View v ){
        StateSingleton.getInstance().updateWorkCycle(activityType, workCycleId, startHour, startMinute, endHour, endMinute, isSet);
        finish();
    }

    public void cancelAction( View v ){
        finish();
    }

}
