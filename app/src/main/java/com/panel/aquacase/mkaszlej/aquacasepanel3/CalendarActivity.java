package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

public class CalendarActivity extends AquaFragmentActivity {

    public CaldroidFragment caldroidFragment;
    private CalendarArrayAdapter array_adapter = null;

    int selectedDay;

    Date lastSelectedDate = null;
    HashSet<Date> eventDates = new HashSet<Date>();

    ListView list;
    FrameLayout calendar_layout;
    CaldroidListener listener;
    View header;
    View footer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.CALENDAR.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callendar);
        initializeView();
    }

    private void initializeView()
    {
        calendar_layout = this.findViewById(R.id.calendar);
        addAddEventButton();
        addBackButton();
        initializeCalendar();
    }

    private void addAddEventButton() {
        header = this.getLayoutInflater().inflate(R.layout.addmorebuttonlayout, null);
    }
    private void addBackButton() {
        footer = this.getLayoutInflater().inflate(R.layout.backbuttonlayout, null);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateLocale();
        recreateFromState();
    }

    private void updateLocale()
    {
        if(initLocale !=  StateSingleton.getInstance().getLocale())
        {
            Locale myLocale = new Locale(StateSingleton.getInstance().getLocale());
            DisplayMetrics dm2 = caldroidFragment.getResources().getDisplayMetrics();
            Configuration conf2 = caldroidFragment.getResources().getConfiguration();
            conf2.locale = myLocale;
            caldroidFragment.getResources().updateConfiguration(conf2,dm2);
            caldroidFragment.refreshView();
        }
    }

    @Override
    public void recreateFromState() {
        selectedDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        caldroidFragment.refreshView();

    }

    @Override
    public void startActivityBasedOnState(Class<?> cls, Bundle extras) {
        Intent i = new Intent(this, cls);
        i.putExtras(extras);
        startActivity(i);
    }

    public void initializeCalendar()
    {
        calendar_layout.setVisibility(View.GONE);
        updateCalendarLocale();
        setupCaldroidFragment();
        loadCaldroidFragment();
        showCaldroidFragment();

        listener = new CaldroidListener()
        {
            @Override
            public void onSelectDate(Date date, View view) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                selectedDay = cal.get(Calendar.DAY_OF_MONTH);

                if(lastSelectedDate != null && eventDates.contains(lastSelectedDate)){
                    caldroidFragment.setBackgroundResourceForDate(R.drawable.calactive, lastSelectedDate);
                    lastSelectedDate = date;
                    caldroidFragment.setBackgroundResourceForDate(R.color.caldroid_sky_blue, date );
                    caldroidFragment.refreshView();
                }
                else{
                    if(lastSelectedDate != null) caldroidFragment.clearBackgroundResourceForDate(lastSelectedDate);
                    lastSelectedDate = date;
                    caldroidFragment.setBackgroundResourceForDate(R.color.caldroid_sky_blue, date );
                    caldroidFragment.refreshView();
                }
            }

            @Override
            public void onChangeMonth(int month, int year) {
                refreshList(month, year);
                caldroidFragment.refreshView();
            }

            @Override
            public void onLongClickDate(Date date, View view) {

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);

                Intent i = new Intent(CalendarActivity.this,CalendarAddActivity.class);
                i.putExtra("DAY",day);
                i.putExtra("MONTH",month);
                i.putExtra("YEAR",year);
                CalendarActivity.this.startActivity(i);
            }
        };

        caldroidFragment.setCaldroidListener(listener);
    }

    private void updateCalendarLocale()
    {
        Locale locale = new Locale(initLocale);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private void setupCaldroidFragment()
    {
        if(caldroidFragment == null)
        {
            caldroidFragment = new CaldroidFragment();
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
            args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
            caldroidFragment.setArguments(args);
        }
    }

    private void loadCaldroidFragment()
    {
        android.support.v4.app.FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();
    }

    private void showCaldroidFragment()
    {
        calendar_layout.setVisibility(View.VISIBLE);
        caldroidFragment.setBackgroundResourceForDate(R.drawable.caltoday, Calendar.getInstance().getTime());
        selectedDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    public void initializeList()
    {
        list = findViewById(R.id.listView);
        if (array_adapter == null)
        {
            array_adapter = new CalendarArrayAdapter(this, StateSingleton.getInstance().getEvents());
            list.setAdapter(array_adapter);
        }
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                CalendarEvent c = (CalendarEvent) parent.getItemAtPosition(position);
                Intent i = new Intent(CalendarActivity.this, CalendarAddActivity.class);
                i.putExtra("DAY", c.getDay());
                i.putExtra("MONTH", c.getMonth());
                i.putExtra("YEAR", c.getYear());
                i.putExtra("TITLE", c.getTitle());
                i.putExtra("EDIT", true);
                i.putExtra("ISALARMSET", c.getAlarm());
                i.putExtra("ALARMHOUR", c.getAlarmHour());
                i.putExtra("ALARMMINUTE", c.getAlarmHour());
                caldroidFragment.clearBackgroundResourceForDate(c.getCalDate());
                eventDates.remove(c.getCalDate());
                CalendarActivity.this.startActivity(i);
            }
        });
        list.addFooterView(footer);
        list.addHeaderView(header);

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        refreshList(month + 1, year);
    }

    public void refreshList(int month, int year)
    {
        if (array_adapter == null) {
            initializeList();
            return;
        }
        array_adapter.setCurrentDate(month,year);
        array_adapter.setEvents(StateSingleton.getInstance().getEvents());
        updateVisibleEventsInCalendar(month, year);
        array_adapter.notifyDataSetChanged();
    }

    private void updateVisibleEventsInCalendar(int month, int year)
    {
        eventDates.clear();
        for (CalendarEvent c : StateSingleton.getInstance().getEvents()){
            if (month == c.getMonth() && year == c.getYear())
            {
                caldroidFragment.setBackgroundResourceForDate(R.drawable.calactive, c.getCalDate());
                eventDates.add(c.getCalDate());
            }
        }
    }

    public void backAction(View v) {
        startActivity(new Intent(this, MainFragmentActivity.class));
        finish();
    }

    public void addAction(View v) {
        Intent i = new Intent(CalendarActivity.this,CalendarAddActivity.class);
        i.putExtra("DAY", selectedDay);
        i.putExtra("MONTH", caldroidFragment.getMonth() - 1);
        i.putExtra("YEAR", caldroidFragment.getYear());
        CalendarActivity.this.startActivity(i);
    }

}
