package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class CalendarAddActivity extends AquaBaseActivity {

    String title;
    int alarmHour, alarmMinute, day,month,year;
    boolean editMode, isAlarmSet;

    LinearLayout timeHideLayout;
    Button removeButton;
    EditText titleInput;
    CheckBox calCheckbox;
    TimePicker timePicker;
    TextView dateField;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.CALENDARADD.value;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_add);

        day = -1; month = -1; year = -1;

        //GET DATA FROM INTENT
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            day =  extras.getInt("DAY");
            month =  extras.getInt("MONTH") + 1;
            year =  extras.getInt("YEAR");
            editMode = extras.getBoolean("EDIT",false);
            if(editMode){
                month -= 1; //1 IS ALREADY ADDED ON CREATION
                title = extras.getString("TITLE","UNSET");
                isAlarmSet = extras.getBoolean("ISALARMSET", false);
                if(isAlarmSet){
                    alarmHour = extras.getInt("ALARMHOUR");
                    alarmMinute = extras.getInt("ALARMMINUTE");
                }
            }
        }

        setInitialState();

        if(editMode)
        {
            setEditState();
        }

        setAddingDate();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    @Override
    public void recreateFromState()
    {
        updatePicker();
    }

    private void setInitialState()
    {
        timeHideLayout = findViewById(R.id.timeHide);
        removeButton = findViewById(R.id.removeButton);
        titleInput = findViewById(R.id.titleInput);
        calCheckbox = findViewById(R.id.calCheckBox);
        timePicker = findViewById(R.id.timePicker);
        dateField = findViewById(R.id.dateField);
    }

    private void setEditState()
    {
        titleInput.setText(title);
        calCheckbox.setChecked(isAlarmSet);
        removeButton.setVisibility(View.VISIBLE);

        if(isAlarmSet){
            timePicker.setCurrentHour(alarmHour);
            timePicker.setCurrentMinute(alarmMinute);
        }
    }

    public void updatePicker()
    {
        timeHideLayout.setVisibility(calCheckbox.isChecked() ? View.VISIBLE : View.INVISIBLE );
    }

    public void setAddingDate()
    {
        timePicker.setIs24HourView(true);

        if(day == -1 || month == -1 || year == -1) return;

        String title;
        if(!editMode){
            if(month < 10) title = getResources().getString(R.string.calAddTitle)+day+".0"+month+"."+year;
            else title = getResources().getString(R.string.calAddTitle)+day+"."+month+"."+year;
        }
        else{
            if(month < 10) title = getResources().getString(R.string.calEditTitle)+day+".0"+month+"."+year ;
            else  title = getResources().getString(R.string.calEditTitle)+day+"."+month+"."+year ;
        }

        dateField.setText( title );
    }

    public void acceptAction( View v )
    {
        if(!editMode){
            StateSingleton.getInstance().addEvent(
                    day,month,year,
                    titleInput.getText().toString(),
                    calCheckbox.isChecked(),
                    timePicker.getCurrentHour(),
                    timePicker.getCurrentMinute() );
        }
        else{
            StateSingleton.getInstance().editEvent(
                    day, month, year, title,
                    titleInput.getText().toString(),
                    calCheckbox.isChecked(),
                    timePicker.getCurrentHour(),
                    timePicker.getCurrentMinute() );
        }
        finish();
    }

    public void cancelAction( View v )
    {
        finish();
    }

    public void removeAction( View v )
    {
        StateSingleton.getInstance().removeEvent(day, month, year, title);
        finish();
    }

    public void checkBoxClicked(View v)
    {
        updatePicker();
    }

}
