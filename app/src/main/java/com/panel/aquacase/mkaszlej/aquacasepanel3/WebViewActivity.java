package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class WebViewActivity extends AquaBaseActivity {

    ImageSwitcher img;
    Button btn;

    int imgCounter, manualType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        manualType = getIntent().getIntExtra("manualtype", 0);
        activity_type = manualType;
        imgCounter = 0;

        initializeView();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    private void initializeView()
    {
        btn = findViewById(R.id.contactButtonWebView);
        img = findViewById(R.id.imageSwitcher);

        img.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                myView.setScaleType(ImageView.ScaleType.FIT_XY);
                myView.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
                return myView;
            }
        });
        img.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_right));
        img.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_left));
        btn.setText(R.string.revertToFactory);
    }

    @Override
    public void recreateFromState()
    {
        img.setImageDrawable(getImageRid());
        btn.setVisibility(manualType == AquariumState.ActivityType.FACTORYRESET.value ? View.VISIBLE: View.GONE);
    }

    public void backButtonAction(View v)
    {
        startActivity(new Intent(this, MainFragmentActivity.class));
        finish();
    }

    public void nextImage(View v)
    {
       imgCounter++;
       try{
           img.setImageDrawable( getImageRid() );
       }
       catch( Exception e ){
           //TODO: Error Image
           img.setImageResource( R.drawable.panelmanual01);
       }
    }

    Drawable getImageRid()
    {
        int maxNumber = 0;
        String imgStr = "";

        if(manualType == AquariumState.ActivityType.GUARANTEEMANUAL.value) {
            imgStr = "guaranteemanual";
            maxNumber = 0;
        }
        if(manualType == AquariumState.ActivityType.FACTORYRESET.value) {
            imgStr = "factorymanual";
            maxNumber = 0;
        }
        else if(manualType == AquariumState.ActivityType.PANELMANUAL.value) {
            imgStr = "panelmanual";
            maxNumber = 15;
        }
        else if(manualType == AquariumState.ActivityType.AQUARIUMMANUAL.value){
            imgStr = "aquariummanual";
            maxNumber = 16;
        }

        if(imgCounter > maxNumber) imgCounter = 0;
        if(imgCounter < 10) imgStr+="0";

        imgStr += Integer.toString(imgCounter);
        return getResources().getDrawable(getResources().getIdentifier(imgStr, "drawable", getPackageName()));
    }

}
