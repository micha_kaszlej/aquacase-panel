package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Locale;

public class SideFragment extends android.support.v4.app.Fragment implements IAquariumStateDependent {

    public static SideFragment newInstance() {
        SideFragment fragment = new SideFragment();
        return fragment;
    }

    public SideFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Locale myLocale = new Locale(StateSingleton.getInstance().getLocale());
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        View v = inflater.inflate(R.layout.fragment_side, container, false);
        //setOnClickListeners(captionText);
        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }


    @Override
    public void recreateFromState() {
        View v = getView();
        if( v != null ){
            setOnClickListeners(v);
            setButtons(v);
        }
    }

    @Override
    public void startActivityBasedOnState(Class<?> cls, Bundle extras) {
        // DO nothing as mainfragmentactivity handles this
    }

    @Override
    public boolean canSyncWithServer() {
        return false;
    }

    private void setButtons( View u ){
        ImageView warranty = (ImageView) u.findViewById(R.id.warranty);
        ImageView aquariummanual = (ImageView) u.findViewById(R.id.aquariummanual);
        ImageView usermanual = (ImageView) u.findViewById(R.id.usermanual);
        ImageView setsettings = (ImageView) u.findViewById(R.id.setsettings);
        ImageView subscription = (ImageView) u.findViewById(R.id.subscription);
        ImageView setpin = (ImageView) u.findViewById(R.id.setpin);
        ImageView setlanguage = (ImageView) u.findViewById(R.id.setlanguage);
        ImageView setbackground = (ImageView) u.findViewById(R.id.setbackground);
        ImageView revert = (ImageView) u.findViewById(R.id.revert);
        ImageView intelligenthome = (ImageView) u.findViewById(R.id.intelligenthome);
        ImageView secret = (ImageView) u.findViewById(R.id.secret);
        ImageView display = (ImageView) u.findViewById(R.id.display);

        //LOAD IMAGES WITH PICASSO DUE TO MEMORY ERRORS
        Context context = getActivity();
        Util.setNormalDrawable(context, warranty, R.drawable.warranty);
        Util.setNormalDrawable(context, aquariummanual, R.drawable.usermanual);
        Util.setNormalDrawable(context, usermanual, R.drawable.usermanual);
        Util.setNormalDrawable(context, setsettings, R.drawable.setsettings);
        Util.setNormalDrawable(context, subscription, R.drawable.subscription);
        Util.setNormalDrawable(context, setpin, R.drawable.setpin);
        Util.setNormalDrawable(context, setlanguage, R.drawable.setlanguage);
        Util.setNormalDrawable(context, setbackground, R.drawable.setbackground);
        Util.setNormalDrawable(context, revert, R.drawable.revert);
        Util.setNormalDrawable(context, intelligenthome, R.drawable.inteligenthome);
        Util.setNormalDrawable(context, secret, R.drawable.setpin);
        Util.setNormalDrawable(context, display, R.drawable.display);
    }

    private void setOnClickListeners( View u ){
        ((LinearLayout) u.findViewById(R.id.guaranteeButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.aquariumManualButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.panelManualButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.serviceButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.pinCodeButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.subscriptionButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.backgroundButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.languageButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.pairPhoneButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.intelligentHomeButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.revertButton)).setOnClickListener( onClickListener );
        ((LinearLayout) u.findViewById(R.id.displayOffButton)).setOnClickListener( onClickListener );
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View v) {

        String locale = StateSingleton.getInstance().getLocale();
        Intent i ;

        switch(v.getId()){
            case R.id.guaranteeButton:

                i = new Intent( getActivity() , WebViewActivity.class );
                if(locale.equals("pl")){
//                    i.putExtra("url", "file:///android_asset/guaranteemanual_pl.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.GUARANTEEMANUAL.value );
                }
                else{
                    //i.putExtra("url", "file:///android_asset/guaranteemanual.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.GUARANTEEMANUAL.value );
                }
                startActivity(i);

                break;
            case R.id.aquariumManualButton:

                i = new Intent( getActivity() , WebViewActivity.class );
                if(locale.equals("pl")){
//                    i.putExtra("url", "file:///android_asset/aquariummanual_pl.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.AQUARIUMMANUAL.value );
                }
                else{
                    //i.putExtra("url", "file:///android_asset/aquariummanual.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.AQUARIUMMANUAL.value );
                }
                startActivity(i);

                break;
            case R.id.panelManualButton:

                i = new Intent( getActivity() , WebViewActivity.class );
                if(locale.equals("pl")){
//                    i.putExtra("url", "file:///android_asset/panelmanual_pl.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.PANELMANUAL.value );
                }
                else{
                    //i.putExtra("url", "file:///android_asset/panelmanual.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.PANELMANUAL.value );
                }
                startActivity(i);

                break;

            case R.id.serviceButton:

                i = new Intent( getActivity() , WebViewActivity.class );
                if(locale.equals("pl")){
                //    i.putExtra("url", "file:///android_asset/servicemanual_pl.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.PANELMANUAL.value );
                }
                else{
//                    i.putExtra("url", "file:///android_asset/servicemanual.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.PANELMANUAL.value );
                }
                i.putExtra("hideContact", false);
                startActivity(i);

                break;

            case R.id.subscriptionButton:

                i = new Intent( getActivity() , WebViewActivity.class );
                if(locale.equals("pl")){
                //    i.putExtra("url", "file:///android_asset/subscriptionmanual_pl.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.PANELMANUAL.value );
                }
                else{
                    //i.putExtra("url", "file:///android_asset/subscriptionmanual.html");
                    i.putExtra("manualtype", AquariumState.ActivityType.PANELMANUAL.value );
                }
                startActivity(i);

                break;
            case R.id.pinCodeButton:

                i = new Intent(getActivity(), PinCodeActivity.class);
                i.putExtra("mode",PinCodeActivity.Mode.SET_NEW_PIN.value);
                startActivity(i);

                break;
            case R.id.pairPhoneButton:

                i = new Intent(getActivity(), ConnectActivity.class);
                startActivity(i);

                break;

            case R.id.intelligentHomeButton:

                i = new Intent(getActivity(), PinCodeActivity.class);
                i.putExtra("mode",PinCodeActivity.Mode.PAIR_INTELLIGENT_HOME);
                startActivity(i);

                break;

            case R.id.displayOffButton:

                i = new Intent(getActivity(), DisplayActivity.class);
                startActivity(i);

                break;

            case R.id.backgroundButton:

                StateSingleton.getInstance().toggleBackground();
                ((MainFragmentActivity)getActivity()).refreshBackground();

                break;
            case R.id.languageButton:
                StateSingleton.getInstance().toggleLocale();
                ((MainFragmentActivity)getActivity()).refreshSideFragment();
                break;
            case R.id.revertButton:
                i = new Intent( getActivity() , WebViewActivity.class );
                if(locale.equals("pl")){
                    i.putExtra("url", "file:///android_asset/revert_pl.html");
                    i.putExtra("fallback", "file:///android_asset/revert_pl.html");
                }
                else{
                    i.putExtra("url", "file:///android_asset/revert.html");
                    i.putExtra("fallback", "file:///android_asset/revert.html");
                }
                i.putExtra("manualtype", AquariumState.ActivityType.FACTORYRESET.value );
                i.putExtra("hideContact", false);
                i.putExtra("showRevert", true);
                startActivity(i);
                break;
        }
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

}
