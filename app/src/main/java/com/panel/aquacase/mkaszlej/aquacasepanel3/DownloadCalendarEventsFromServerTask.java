package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DownloadCalendarEventsFromServerTask extends AsyncTask<String, String, String>
{
    private String url_get_events;

    private final String TAG = "AQUACASE";
    private final String PREFIX = "[DownloadCalendarEventsFromServerTask] ";
    private final String TAG_SUCCESS = "success";
    private final String METHOD = "POST";
    private final String USER_ID = "userId";
    private final String USER_CODE = "userCode";
    private final String CALENDAR_EVENTS = "calendar_events";

    DownloadCalendarEventsFromServerTask(String url) {
        super();
        url_get_events = url;
        if (url.length() == 0) throw new AssertionError();
    }

    private JSONObject makeHttpRequest()
    {
        List<NameValuePair> cal_events = new ArrayList<NameValuePair>();
        cal_events.add(new BasicNameValuePair(USER_ID, Integer.toString(StateSingleton.getInstance().getState().userId)));
        cal_events.add(new BasicNameValuePair(USER_CODE, Integer.toString(StateSingleton.getInstance().getState().userCode)));
        return new JSONParser().makeHttpRequest(url_get_events, METHOD, cal_events);
    }

    private boolean isRequestOk(JSONObject json)
    {
        if (json != null) {
            Log.d(TAG, PREFIX + "server responded: " + json.toString());
            return true;
        }
        Log.d(TAG, PREFIX + "FAIL! message was null");
        return false;
    }

    private void processServerResponse(JSONObject json) throws JSONException
    {
        if (json.getInt(TAG_SUCCESS) == 1)
        {
            processSuccessMessage(json);
            Log.d(TAG, PREFIX + "Received calendar events: " + json.toString());
        }
        else
            processFailureMessage();
    }

    private void processSuccessMessage(JSONObject json) throws JSONException {
        updateAquariumState(json.getJSONArray(CALENDAR_EVENTS));
    }

    private void updateAquariumState(JSONArray events)
    {
        StateSingleton.getInstance().getState().updateCalendarEvents(events);
        NetworkSingleton.getInstance().setIsConnected(true);
    }

    private void processFailureMessage()
    {
        NetworkSingleton.getInstance().setIsConnected(false);
    }

    protected String doInBackground(String... params)
    {
        try {
            JSONObject json = makeHttpRequest();
            if (isRequestOk(json))
                processServerResponse(json);
        } catch (Exception e) {
            processFailureMessage();
            e.printStackTrace();
        }
        return null;
    }
}