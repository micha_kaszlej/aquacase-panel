package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ConnectActivity extends AquaBaseActivity {

    TextView id;
    TextView code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activity_type = AquariumState.ActivityType.CONNECT.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
        initializeView();
    }

    @Override
    public void onResume() {
        super.onResume();
        recreateFromState();
    }

    private void initializeView(){
        id = findViewById(R.id.userIdTextView);
        code = findViewById(R.id.userCodeTextView);
    }

    @Override
    public void recreateFromState() {
        id.setText( Integer.toString(StateSingleton.getInstance().getUserId()) );
        code.setText( Integer.toString(StateSingleton.getInstance().getUserCode()) );
    }

    public void backAction(View v){
        if(NetworkSingleton.getInstance().isConnected()) {
            startActivity(new Intent(this, LockActivity.class));
        } else {
            startActivity(new Intent(this, NoConnectionErrorActivity.class));
        }
        StateSingleton.getInstance().resetState(this);
        finish();
    }

    public void setNewCode(View v){
        Intent i = new Intent(this, PinCodeActivity.class);
        i.putExtra("mode",PinCodeActivity.Mode.SET_REMOTE_ACCESS_CODE.value);
        startActivity(i);
    }

    public void setSerial(View v){
        Intent i = new Intent(this, PinCodeActivity.class);
        i.putExtra("mode",PinCodeActivity.Mode.SET_SERIAL_NUMBER.value);
        startActivity(i);
    }

}

