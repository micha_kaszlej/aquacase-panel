package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class ThermoActivity extends AquaBaseActivity {

    ImageView screenIcon;
    SeekBar thermoBar;
    TextView desiredTemp;
    TextView currentTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.THERMO.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thermo);
        intializeView();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    private void intializeView()
    {
        screenIcon = findViewById(R.id.screenIcon);
        thermoBar = findViewById(R.id.thermoBar);
        desiredTemp = findViewById(R.id.desiredTemp);
        currentTemp = findViewById(R.id.currentTemp);

        thermoBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                StateSingleton.getInstance().setDesiredTemperature(seekBar.getProgress() + 10);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
            {
                int celsius = progress+10;
                desiredTemp.setText(String.format( "%d"+(char) 0x00B0+"C", celsius ));
            }
        });

        Util.setNormalDrawable(getApplicationContext(), screenIcon, R.drawable.thermoscreenicon);
    }


    @Override
    public void recreateFromState()
    {
        thermoBar.setProgress( StateSingleton.getInstance().getDesiredTemperature() - 10);
        desiredTemp.setText(String.format("%d"+(char) 0x00B0+"C", thermoBar.getProgress()+10  ));
        currentTemp.setText(String.format("%d"+(char) 0x00B0+"C", StateSingleton.getInstance().getCurrentTemperature() ));
    }

    public void backAction(View v)
    {
        startActivity(new Intent(this, MainFragmentActivity.class));
        finish();
    }

}

