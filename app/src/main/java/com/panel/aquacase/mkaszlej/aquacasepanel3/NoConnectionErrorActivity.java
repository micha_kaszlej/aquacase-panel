package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class NoConnectionErrorActivity extends AquaBaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activity_type = AquariumState.ActivityType.NO_CONNECTION.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void recreateFromState() {}

    public void buttonAction(View v)    {
        StateSingleton.getInstance().resetState(this);
        if(NetworkSingleton.getInstance().isConnected()) {
            Intent start = new Intent( this, LockActivity.class );
            startActivity(start);
            finish();
        }
    }

    public void buttonSetAuthData(View v)
    {
            Intent start = new Intent( this, ConnectActivity.class );
            this.startActivity(start);
            finish();
    }
}

