package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PinCodeActivity extends AquaBaseActivity {

    LinearLayout layout;
    TextView display; //displays pin as XXXX

    private String pincode; //<- holds entered digits as string

    private boolean firstTouch; //<- flag used to reset display
    private int targetPin = 0; //for VERIFY MODE

    enum Mode {

        ENTER_PIN(0),
        SET_NEW_PIN(1),
        VERIFY_NEW_PIN(2),
        SET_REMOTE_ACCESS_CODE(3),
        PAIR_INTELLIGENT_HOME(4),
        SET_SERIAL_NUMBER(5);

        public int value;

        Mode(int value){
            this.value = value;
        }

        public static Mode getMode(int mode_index) {
            for (Mode m : Mode.values()) {
                if (m.value == mode_index) return m;
            }
            throw new IllegalArgumentException("Mode not found.");
        }
    }
    private Mode activity_mode = Mode.ENTER_PIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activity_type = AquariumState.ActivityType.PIN.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_code);
        initializeView(getIntent().getIntExtra("mode", 0));
    }

    private void initializeView(int mode_value)
    {
        layout = findViewById(R.id.pinButtonsGroup);
        display = findViewById(R.id.pinLabel);
        activity_mode = Mode.getMode(mode_value);
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    @Override
    public void recreateFromState() {
        resetCurrentState();

        layout.setVisibility( View.GONE );
        display.setText(R.string.enterPin);

        if (activity_mode == Mode.SET_NEW_PIN){
            display.setText(R.string.setNewPin);
            if(layout!=null) layout.setVisibility( View.VISIBLE );
        }
        else if (activity_mode == Mode.SET_REMOTE_ACCESS_CODE){
            display.setText(R.string.pairPhonePin);
        }
        else if (activity_mode == Mode.PAIR_INTELLIGENT_HOME){ //NOT USED
            display.setText(R.string.pairIntelligentHomePin);
        }
        else if (activity_mode == Mode.SET_SERIAL_NUMBER){
            display.setText(R.string.setNewSerialTitle);
        }
    }

    private void resetCurrentState() {
        pincode = "";
        display.setText("");
        firstTouch = true;
    }

    private boolean updateActivityState() {
        if(firstTouch)
        {
            display.setText("");
            firstTouch = false;
        }
        if(display.getText().length() >= 4)
        {
            resetCurrentState();
            display.setText(R.string.tooManyDigits);
            return false;
        }
        return true;
    }

    public void pin1(View v){
        if (!updateActivityState()) return;
        pincode += "1";
        display.append("X");
    }

    public void pin2(View v){
        if (!updateActivityState()) return;
        pincode += "2";
        display.append("X");
    }

    public void pin3(View v){
        if (!updateActivityState()) return;
        pincode += "3";
        display.append("X");
    }

    public void pin4(View v){
        if (!updateActivityState()) return;
        pincode += "4";
        display.append("X");
    }

    public void pin5(View v){
        if (!updateActivityState()) return;
        pincode += "5";
        display.append("X");
    }

    public void pin6(View v){
        if (!updateActivityState()) return;
        pincode += "6";
        display.append("X");
    }

    public void pin7(View v){
        if (!updateActivityState()) return;
        pincode += "7";
        display.append("X");
    }

    public void pin8(View v){
        if (!updateActivityState()) return;
        pincode += "8";
        display.append("X");
    }

    public void pin9(View v){
        if (!updateActivityState()) return;
        pincode += "9";
        display.append("X");
    }

    public void pin0(View v){
        if (!updateActivityState()) return;
        pincode += "0";
        display.append("X");
    }

    //EXIT ON DOUBLE X, OR ON X CLICKED WHEN SCREEN EMPTY
    public void pinx(View v){
        if( pincode == "" ){
            finish();
        }
        else{
            resetCurrentState();
        }
    }

    public void pinReset(View v){
        StateSingleton.getInstance().setPinCode(this,  -1 );
        finish();
    }

    //ACCEPT BUTTON
    public void pinOk(View v)
    {
        if(firstTouch) return;
        if( activity_mode == Mode.ENTER_PIN ) {
            if(isPinOk()) {
                StateSingleton.getInstance().PinCodeEntered();
                startActivity(new Intent(this, MainFragmentActivity.class));
                finish();
            } else {
                resetCurrentState();
                display.setText("WRONG PIN!");
            }
        }
        else if(activity_mode == Mode.SET_NEW_PIN) {
            setNewPin();
        }
        else if(activity_mode == Mode.VERIFY_NEW_PIN) {
            verifyPin();
        }
        else if(activity_mode == Mode.SET_REMOTE_ACCESS_CODE){
            StateSingleton.getInstance().setUserCode( this, Integer.parseInt(pincode) );
            finish();
        }
        else if(activity_mode == Mode.PAIR_INTELLIGENT_HOME){
            finish();
        }
        else if(activity_mode == Mode.SET_SERIAL_NUMBER){ //NEW SERIAL
            StateSingleton.getInstance().setSerialNumber( this, Integer.parseInt(pincode) );
            finish();
        }
    }

    private boolean isPinOk() {
        int pin = StateSingleton.getInstance().getPinCode();
        return pin == -1 || Integer.parseInt(pincode) == pin;
    }

    private void setNewPin()
    {
        if (display.getText().length() != 4) {
            display.setText(R.string.tooManyDigits);
            targetPin = -1;
        }
        else {
            activity_mode = Mode.VERIFY_NEW_PIN;
            targetPin = Integer.parseInt(pincode);
            display.setText(R.string.verifyPin);
        }
        resetCurrentState();
    }

    private void verifyPin()
    {
        if (targetPin == Integer.parseInt(pincode)) {
            StateSingleton.getInstance().setPinCode(this, targetPin);
            finish();
        }
        else {
            activity_mode = Mode.SET_NEW_PIN;
            targetPin = -1;
            resetCurrentState();
            display.setText(R.string.errorSetNewPin);
        }
    }

}
