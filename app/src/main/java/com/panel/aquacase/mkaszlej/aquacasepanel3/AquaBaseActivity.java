package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Locale;

public abstract class AquaBaseActivity extends Activity implements IAquariumStateDependent
{
    String initLocale;
    int activity_type = -1;


    Handler syncWithServerHander = new Handler();
    private Runnable startSyncTaskRunnable = new SyncWithServerRunnable(this, syncWithServerHander);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        initLocale = StateSingleton.getInstance().getLocale();
        super.onCreate(savedInstanceState);
     }

     private void registerHandler()
     {
         canSync = true;
         syncWithServerHander.postDelayed(startSyncTaskRunnable, 500);
     }

     private void unregisterHandler()
     {
         canSync = false;
         syncWithServerHander.removeCallbacks(startSyncTaskRunnable);
         syncWithServerHander.removeCallbacksAndMessages(null);
     }

     private boolean canSync = true;
     public boolean canSyncWithServer()
     {
         return canSync;
     }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        updateLocale();
        super.onResume();

        StateSingleton.getInstance().setCurrentScreenId(this,activity_type);
        refreshBackground();
        registerHandler();

        if(isNetworkError()){
            interruptWithNetworkError();
        }
    }

    private boolean isNetworkError()
    {
        return !NetworkSingleton.getInstance().isConnected() && shouldShowNoConnectionWarning();
    }


    private void interruptWithNetworkError()
    {
        startActivity(new Intent(this, NoConnectionErrorActivity.class));
        finish();
    }


    private boolean shouldShowNoConnectionWarning() {
        return activity_type != AquariumState.ActivityType.NO_CONNECTION.value &&
               activity_type != AquariumState.ActivityType.CONNECT.value &&
               activity_type != AquariumState.ActivityType.ANIMATIONSHOW.value &&
               activity_type != AquariumState.ActivityType.PIN.value;
    }

    private void updateLocale() {
        if (initLocale !=  StateSingleton.getInstance().getLocale()) {
            Locale myLocale = new Locale(StateSingleton.getInstance().getLocale());
            DisplayMetrics dm = getResources().getDisplayMetrics();
            Configuration conf = getResources().getConfiguration();
            conf.locale = myLocale;
            getResources().updateConfiguration(conf, dm);
            initLocale = StateSingleton.getInstance().getLocale();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterHandler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void refreshBackground()
    {
        Picasso.with(getApplicationContext()).load(StateSingleton.getInstance().getBackground()).into(new Target()
        {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                RelativeLayout layout = findViewById(R.id.mainLayout);
                layout.setBackground(new BitmapDrawable(getApplicationContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                if(errorDrawable != null)
                    Log.d("AQUACASE", errorDrawable.toString());
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null)
                    Log.d("AQUACASE", placeHolderDrawable.toString());
            }
        });
    }

    @Override
    public void startActivityBasedOnState(Class<?> cls, Bundle extras) {
        Intent i = new Intent(this, cls);
        i.putExtras(extras);
        startActivity(i);
    }
}
