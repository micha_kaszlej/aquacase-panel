package com.panel.aquacase.mkaszlej.aquacasepanel3;

public class NetworkSingleton
{
    private static NetworkSingleton mInstance = null;
    final String server_url = "http://aquacase.pl/panel/";
    final String get_last_message_id_script_name = "get_last_message_id.php";
    final String get_state_script_name = "read_state_app.php";
    final String get_work_cycles_script_name = "read_cycles_app.php";
    final String send_state_script_name = "save_state_app.php";
    final String send_cycle_script_name = "save_cycle_app.php";
    final String get_calendar_events_script_name = "read_cld_app.php";
    final String send_calendar_event_script_name = "sync_cld_app.php";

    private boolean isConnectionEstablished = false;

    public static NetworkSingleton getInstance()
    {
        if(mInstance == null)
            mInstance = new NetworkSingleton();
        return mInstance;
    }

    public void setIsConnected(boolean value) {
        isConnectionEstablished = value;
    }

    public boolean isConnected()
    {
        return isConnectionEstablished;
    }

    public void downloadLastServerMessageId() {
        new GetLastMessageIdTask(server_url + get_last_message_id_script_name).execute();
    }

    public void downloadStateFromServer(IAquariumStateDependent activity) {
        new DownloadStateFromServerTask(server_url + get_state_script_name, activity).execute();
    }

    public void downloadCyclesFromServer() {
        new DownloadWorkCyclesTask(server_url + get_work_cycles_script_name).execute();
    }

    public void downloadCalendarEventsFromServer() {
        new DownloadCalendarEventsFromServerTask(server_url + get_calendar_events_script_name).execute();
    }

    public void sendStateToServer() {
        new UploadStateToServerTask(server_url + send_state_script_name).execute();
    }

    public void sendWorkCycleToServer(WorkCycle w) {
        new UploadWorkCycleTask(server_url + send_cycle_script_name, w).execute();
    }

    public void sendCalendarEventToServer(CalendarEvent c) {
        new UploadCalendarEventTask(server_url + send_calendar_event_script_name, c).execute();
    }
}
