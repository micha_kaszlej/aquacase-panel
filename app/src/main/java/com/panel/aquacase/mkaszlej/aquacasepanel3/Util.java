package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by mkaszlej on 8/31/2016.
 */
public class Util
{

    public static void setNormalDrawable(Context context, ImageView viewToSet, int drawableId){
        setNormalDrawable(context,viewToSet,drawableId,256,256);
    }

    public static void setStateDrawable(final Context context, final ImageView viewToSet, final int normal, final int active){
        setStateDrawable(context,viewToSet,normal,active,256,256);
    }

    //----------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------

    public static void setNormalDrawable(Context context, ImageView viewToSet, int drawableId, int width, int height){
        try{
            Picasso.with(context).load(drawableId).resize(width,height).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.transparent).into(viewToSet);
        }
        catch (OutOfMemoryError out)
        {
            // We are screwed here...
        }
    }

    /*
        I am really surprised that this is working....
     */
    public static void setStateDrawable(final Context context, final ImageView viewToSet, final int normal, final int active, final int width, final int height){
        try{
            new SetDrawableTask(context, viewToSet, normal, active, width, height).execute();
        }
        catch (Exception e)
        {
            Log.e("AQUACASE", "CANT START ASYNC TASK!");
        }
    }
}

