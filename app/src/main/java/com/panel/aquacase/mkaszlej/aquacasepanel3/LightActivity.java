package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class LightActivity extends AquaBaseActivity {

    ImageView bulbIcon;
    ImageButton toggleLightButton;
    SeekBar lumosityBar;
    TextView seekLabel;
    TextView toggleLabel;

    boolean is_during_power_change = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.LIGHT.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);
        intializeView();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    @Override
    public void recreateFromState()
    {
        if (is_during_power_change)
            return;

        boolean is_light_active = StateSingleton.getInstance().isLightActive();

        bulbIcon.setSelected(is_light_active);
        toggleLightButton.setSelected(is_light_active);
        lumosityBar.setVisibility(is_light_active ? View.VISIBLE : View.GONE);
        seekLabel.setVisibility(is_light_active ? View.VISIBLE : View.GONE);
        toggleLabel.setText(is_light_active ? R.string.disable : R.string.enable);

        lumosityBar.setProgress(StateSingleton.getInstance().getLightPower() );
    }

    private void intializeView()
    {
        seekLabel = findViewById(R.id.lumosityDesc);
        toggleLabel = findViewById(R.id.enableLabel);

        bulbIcon = findViewById( R.id.screenIcon );
        toggleLightButton = findViewById( R.id.toggleLight );

        Util.setStateDrawable(getApplicationContext(),bulbIcon,R.drawable.lightscreenicon, R.drawable.lightscreenicon1, 256,256);
        Util.setStateDrawable(getApplicationContext(),toggleLightButton,R.drawable.lightscreenicon, R.drawable.lightscreenicon1,256,256);

        lumosityBar = findViewById(R.id.lumosityBar);
        lumosityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                StateSingleton.getInstance().setLightPower(seekBar.getProgress());
                is_during_power_change = false;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                is_during_power_change = true;
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        });
    }

    public void lightEnableButton(View v)
    {
        StateSingleton.getInstance().setLightActive(toggleLightButton.isSelected() ? 0 : 1);
        recreateFromState();
    }

    public void backAction(View v)
    {
        startActivity(new Intent(this, MainFragmentActivity.class));
        finish();
    }

}
