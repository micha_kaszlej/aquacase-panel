package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import java.util.Locale;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public abstract class AquaFragmentActivity extends FragmentActivity implements IAquariumStateDependent {

    String initLocale;
    int activity_type = -1;

    Handler syncWithServerHander = new Handler();
    private Runnable startSyncTaskRunnable = new SyncWithServerRunnable(this, syncWithServerHander);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        initLocale = StateSingleton.getInstance().getLocale();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        registerHandler();
        if (initLocale !=  StateSingleton.getInstance().getLocale())
        {
            Locale myLocale = new Locale(StateSingleton.getInstance().getLocale());
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
            recreate();
        }
        super.onResume();

        StateSingleton.getInstance().setCurrentScreenId(getApplicationContext(),activity_type);

        refreshBackground();

        if(isNetworkError()){
            interruptWithNetworkError();
        }
    }

    private boolean isNetworkError()
    {
        return !NetworkSingleton.getInstance().isConnected();
    }


    private void interruptWithNetworkError()
    {
        startActivity(new Intent(this, NoConnectionErrorActivity.class));
        finish();
    }


    private void registerHandler()
    {
        canSync = true;
        syncWithServerHander.postDelayed(startSyncTaskRunnable, 500);
    }

    private void unregisterHandler()
    {
        canSync = false;
        syncWithServerHander.removeCallbacks(startSyncTaskRunnable);
        syncWithServerHander.removeCallbacksAndMessages(null);
    }

    private boolean canSync = true;
    public boolean canSyncWithServer()
    {
        return canSync;
    }

    @Override
    protected void onPause() {
        unregisterHandler();
        super.onPause();
    }

    public void refreshBackground()
    {
        final RelativeLayout layout = findViewById(R.id.mainLayout);
        final Context context = getApplicationContext();

        Picasso.with(context).load(StateSingleton.getInstance().getBackground()).into(new Target(){

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                layout.setBackground(new BitmapDrawable(context.getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                Log.d("TAG", "FAILED");
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                Log.d("TAG", "Prepare Load");
            }
        });
    }
}
