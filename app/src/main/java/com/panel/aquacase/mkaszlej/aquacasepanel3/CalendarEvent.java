package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.Debug;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mkaszlej on 2016-02-22.
 */
public class CalendarEvent {

    private float timestamp;
    private int num;
    private int task_id;
    private boolean delete_task;
    private boolean is_aquarium;
    private boolean aqua_ack;
    private boolean mobile_ack;
    private String date_string;
    private String time_string;
    private boolean alarm;
    private String txt;

    private int alarm_hour;
    private int alarm_minute;

    private int day, month, year;

    private Date cal_date;

    CalendarEvent(int day, int month, int year, String title) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.txt = title;
        this.timestamp = System.currentTimeMillis();
        this.num = -1;
        this.task_id = -1;
        this.aqua_ack = false;
        this.mobile_ack = true;
        this.alarm = false;
        this.date_string = getDateForSort();
        this.time_string = "";
        parseDateString();
    }

    CalendarEvent(float timestamp, int num, int task_id, boolean delete_task, boolean is_aquarium, boolean aqua_ack, boolean mobile_ack, boolean alarm, String date_string, String time_string, String txt){
        this.timestamp = timestamp;
        this.num = num;
        this.task_id = task_id;
        this.delete_task = delete_task;
        this.is_aquarium = is_aquarium;
        this.aqua_ack = aqua_ack;
        this.mobile_ack = mobile_ack;
        this.alarm = alarm;
        this.date_string = date_string;
        this.time_string = time_string;
        this.txt = txt;
        parseDateString();
        parseTimeString();
    }

    private void parseDateString()
    {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            cal_date = sdf.parse(date_string);
            Calendar c = Calendar.getInstance();
            c.setTime(cal_date);
            day = c.get(Calendar.DAY_OF_MONTH);
            month = c.get(Calendar.MONTH) + 1;
            year = c.get(Calendar.YEAR);
        }
        catch (Exception e)
        {
            Log.e("AQUACASE","Cant parse data: " + date_string);
        }
    }

    private void parseTimeString()
    {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date date = sdf.parse(time_string);
            alarm_hour = date.getHours() ;
            alarm_minute = date.getMinutes();
        }
        catch (Exception e)
        {
            Log.e("AQUACASE","Cant parse data: " + date_string);
        }
    }

    public void setAlarm(int h , int m){
        alarm = true;
        alarm_hour = h;
        alarm_minute = m;
        time_string = timeToString();
    }

    private String timeToString()
    {
        return String.format("%02d:%02d:00", alarm_hour, alarm_minute);
    }

    public void unsetAlarm(){
        alarm = false;
    }

    public void setTitle( String newTitle ){
        this.txt = newTitle;
    }

    public String getDate(){
        return getDateForSort();
    }

    public String getDateForSort(){
        String dayString = ""+day;
        if(day<10) dayString = "0"+day;
        String monthString = ""+month;
        if(month<10) monthString = "0"+month;
        return year+"-"+monthString+"-"+dayString;
    }

    public Date getCalDate()
    {
        return cal_date;
    }

    public int getDay(){
        return day;
    }
    public int getMonth(){
        return month;
    }
    public int getYear(){
        return year;
    }

    public int getAlarmHour() { return alarm_hour; }
    public int getAlarmMinute() { return alarm_minute; }

    public String getTitle(){
        return txt;
    }

    public String getAlarm(){ if(!alarm) return ""; else return time_string; }

    public boolean isAlarmSet() { return alarm; }

    public String toString(){
        return date_string + " - " + time_string;
    }

    public String toJSON(){
        return "{\"day\":"+day+",\"month\":"+month+",\"year\":"+year+",\"title\":"+txt+",\"alarm\":"+alarm+",\"alarmHour\":"+alarm_hour+",\"alarmMinute\":"+alarm_minute+"},";
    }

    @Override
    public boolean equals(Object o) {
        CalendarEvent c = (CalendarEvent) o;
        if( c.getDay() == this.day && c.getMonth() == this.month && c.getYear() == this.year && c.getTitle().equals( this.txt )) return true;
        return false;
    }

    public ArrayList<NameValuePair> getData(){

        ArrayList<NameValuePair> l = new ArrayList<>();
        l.add(new BasicNameValuePair("userId", Integer.toString(StateSingleton.getInstance().getUserId())));
        l.add(new BasicNameValuePair("aquariumId", Integer.toString(StateSingleton.getInstance().getAndIncrementMessageId())));
        l.add(new BasicNameValuePair("userCode", Integer.toString(StateSingleton.getInstance().getUserCode())));
        l.add(new BasicNameValuePair("timestamp", String.format("%d", (long)timestamp)));
        l.add(new BasicNameValuePair("num", Integer.toString(num)  ));
        l.add(new BasicNameValuePair("taskId", Integer.toString(task_id)));
        l.add(new BasicNameValuePair("deleteTask", Boolean.toString(delete_task)));
        l.add(new BasicNameValuePair("isAquarium", Boolean.toString(is_aquarium)));
        l.add(new BasicNameValuePair("aquaAck", Boolean.toString(aqua_ack)));
        l.add(new BasicNameValuePair("mobileAck", Boolean.toString(mobile_ack)));
        l.add(new BasicNameValuePair("date", date_string));
        l.add(new BasicNameValuePair("time", time_string));
        l.add(new BasicNameValuePair("alarm", Boolean.toString(alarm)));
        l.add(new BasicNameValuePair("txt", txt));
        return l;
    }
}
