package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class SetDrawableTask extends AsyncTask<String, String, String>
{
    private  Context context;
    private ImageView viewToSet;
    final StateListDrawable states = new StateListDrawable();
    private int active, normal, width, height;

    SetDrawableTask(final Context c, final ImageView view, final int normal, final int active, final int width, final int height)
    {
        context = c;
        viewToSet = view;
        this.active = active;
        this.normal = normal;
        this.width = width;
        this.height = height;

    }

    @Override
    protected String doInBackground(String... params)
    {
       try{
            Bitmap normalBmp = Picasso.with(context).load(normal).resize(width,height).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.transparent).get();
            Bitmap activeBmp = Picasso.with(context).load(active).resize(width,height).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.transparent).get();

            Drawable normalDrawable = new BitmapDrawable(context.getResources(), normalBmp);
            Drawable activeDrawable = new BitmapDrawable(context.getResources(), activeBmp);

            states.addState(new int[] { -android.R.attr.state_selected, android.R.attr.state_pressed} ,activeDrawable);
            states.addState(new int[] { android.R.attr.state_selected, android.R.attr.state_pressed} ,normalDrawable );
            states.addState(new int[] { android.R.attr.state_selected}, activeDrawable );
            states.addState(new int[] { -android.R.attr.state_selected}, normalDrawable );
        }
        catch ( Exception e ) {
            //... We are screwed here... or not.. ?
        }
        return null;
    }

    @Override
    protected void onPostExecute(String o)
    {
        viewToSet.setImageDrawable(states);
    }
}

