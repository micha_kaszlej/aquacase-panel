package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UploadStateToServerTask extends AsyncTask<String, String, String>
{
    private String url_get_state;

    private final String TAG = "AQUACASE";
    private final String PREFIX = "[UploadStateToServerTask] ";
    private final String TAG_SUCCESS = "success";
    private final String METHOD = "POST";

    UploadStateToServerTask(String url) {
        super();
        url_get_state = url;
        if (url.length() == 0) throw new AssertionError();
    }

    private boolean hasUpToDateState() {
        int server_message = StateSingleton.getInstance().getServerLastMessageId();
        int current_message = StateSingleton.getInstance().getMessageId();
        return  server_message <= current_message;
    }

    private JSONObject makeHttpRequest() {
        if (!NetworkSingleton.getInstance().isConnected() || !hasUpToDateState()) return null;
        StateSingleton.getInstance().getState().increaseMessageId();
        ArrayList<NameValuePair> message_to_server = StateSingleton.getInstance().getState().serialize();
        Log.d(TAG, PREFIX + "message: " + message_to_server.toString());
        return new JSONParser().makeHttpRequest(url_get_state, METHOD, message_to_server);
    }

    private boolean isRequestOk(JSONObject json)
    {
        if (json != null) {
            if(JSONParser.server_response_code != 200) {
                Log.d(TAG, PREFIX + "HTTP error: " + JSONParser.server_response_code);
                return false;
            }
            Log.d(TAG, PREFIX + "server responded: " + json.toString());
            return true;
        }
        Log.d(TAG, PREFIX + "FAIL! message was null");
        return false;
    }

    private void processServerResponse(JSONObject json) throws JSONException
    {
        if (json.getInt(TAG_SUCCESS) == 1) {
            processSuccessMessage();
        }
        else
            processFailureMessage();
    }

    private void processSuccessMessage() {
        StateSingleton.getInstance().setServerLastMessageId(StateSingleton.getInstance().getMessageId());
        NetworkSingleton.getInstance().setIsConnected(true);
    }

    private void processFailureMessage()
    {
//        NetworkSingleton.getInstance().downloadStateFromServer(); //TODO! Start network error info activiy
    }

    protected String doInBackground(String... params)
    {
        try {
            JSONObject json = makeHttpRequest();
            if (isRequestOk(json))
                processServerResponse(json);
        } catch (Exception e) {
            processFailureMessage();
            e.printStackTrace();
        }
        return null;
    }
}