package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class DisplayActivity extends AquaBaseActivity {

    SeekBar displBar;
    SeekBar lockBar;
    TextView dispTimeLock;
    TextView dispTimeOff;
    TextView delayTime;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.DISPLAY.value;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        initializeView();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    private void initializeView()
    {
        displBar = findViewById(R.id.displayOffBar);
        lockBar = findViewById(R.id.displayLockBar);
        dispTimeLock = findViewById(R.id.lockTime);
        dispTimeOff = findViewById(R.id.delayTime);
        delayTime = (TextView) findViewById(R.id.delayTime);

        displBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                StateSingleton.getInstance().setDisplayOffTime(progress);
                if(progress > 0) dispTimeOff.setText(String.format("%d min",progress));
                else dispTimeOff.setText(R.string.alwayson);
            }
        });

        lockBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                StateSingleton.getInstance().setDisplayLockTime(progress);
                if(progress > 0) dispTimeLock.setText(progress+" min");
                else dispTimeLock.setText(R.string.never);
            }
        });
    }

    @Override
    public void recreateFromState() {
        displBar.setProgress(StateSingleton.getInstance().getDisplayOffTime());
        lockBar.setProgress( StateSingleton.getInstance().getDisplayLockTime() );

        if (displBar.getProgress() > 0) dispTimeOff.setText(String.format("%d min" , displBar.getProgress()));
        else dispTimeOff.setText(R.string.alwayson);

        if (lockBar.getProgress()>0) dispTimeLock.setText(String.format("%d min", lockBar.getProgress() ));
        else dispTimeLock.setText(R.string.never);
    }

    public void backAction(View v)
    {
        startActivity(new Intent(this, MainFragmentActivity.class));
        finish();
    }
}