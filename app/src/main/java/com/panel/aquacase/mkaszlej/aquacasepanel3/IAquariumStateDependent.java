package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;

public interface IAquariumStateDependent {
    boolean canSyncWithServer();
    void recreateFromState();
    void startActivityBasedOnState(Class<?> cls, Bundle extras);
}
