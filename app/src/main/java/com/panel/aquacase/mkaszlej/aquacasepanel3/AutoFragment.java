package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class AutoFragment extends android.support.v4.app.Fragment implements IAquariumStateDependent{

    ImageButton lightButton;
    ImageButton rgbButton;
    ImageButton manualButton;
    ImageButton lockButton;

    public static AutoFragment newInstance() {
        AutoFragment fragment = new AutoFragment();
        return fragment;
    }

    public AutoFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_auto, container, false);
        intializeView(v);
        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void intializeView(View v)
    {
        lightButton = v.findViewById(R.id.lightButton);
        rgbButton = v.findViewById(R.id.rgbButton);
        lockButton = v.findViewById(R.id.lockButton);
        manualButton = v.findViewById(R.id.autoButton);

        Context context = getActivity();
        Util.setStateDrawable(context, lightButton, R.drawable.light0, R.drawable.light2, 512, 512);
        Util.setStateDrawable(context, rgbButton, R.drawable.rgb0, R.drawable.rgb2, 512, 512);
        Util.setStateDrawable(context, lockButton, R.drawable.lock0, R.drawable.lock1, 512, 512);
        Util.setStateDrawable(context, manualButton, R.drawable.manual0, R.drawable.manual1,  512, 512);

        lightButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent( getActivity() , ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.LIGHT.value);
                startActivity(i);
                return true;
            }
        });
        lightButton.setOnClickListener( onClickListener );

        rgbButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent( getActivity() , ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.RGB.value);
                startActivity(i);
                return true;
            }
        });
        rgbButton.setOnClickListener( onClickListener );

        lockButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                lockButtonAction( v );
                return true;
            }
        });
        lockButton.setOnClickListener( onClickListener );

        manualButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                autoAction( v );
                return true;
            }
        });
        manualButton.setOnClickListener( onClickListener );
    }

    @Override
    public void recreateFromState()
    {
        StateSingleton s = StateSingleton.getInstance();
        lightButton.setSelected(s.isLightActive());
        rgbButton.setSelected(s.isRgbActive() > 0);
    }

    @Override
    public void startActivityBasedOnState(Class<?> cls, Bundle extras) {
        // DO nothing as mainfragmentactivity handles this
    }

    @Override
    public boolean canSyncWithServer() {
        return false;
    }

    public void autoAction(View v){
        StateSingleton.getInstance().setMode(AquariumState.ActivityType.AUTO.value );
        ((MainFragmentActivity) getActivity()).refreshFragments();

        StateSingleton.getInstance().setCurrentScreenId(getActivity(), AquariumState.ActivityType.AUTO.value);
    }

    public void rgbButtonAction( View v ) {
        startActivity(new Intent(getActivity(), RgbActivity.class));
    }

    public void lightButtonAction( View v ){
        startActivity(new Intent(getActivity(), LightActivity.class));
    }

    public void lockButtonAction( View v ){
        startActivity(new Intent(getActivity(), LockActivity.class));
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
        switch(v.getId()){
            case R.id.autoButton:
                autoAction( v );
                break;
            case R.id.rgbButton:
                rgbButtonAction( v );
                break;
            case R.id.lightButton:
                lightButtonAction( v );
                break;
            case R.id.lockButton:
                lockButtonAction( v );
                break;
        }
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

}
