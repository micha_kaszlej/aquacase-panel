package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Locale;

/*
CO W NOWEJ KOMÓRCE REFERENCYJNEJ:
BUGI:

- Poprawione ikonyp w bmp
- Zwiększenie ZOOMA w manual
- Po włączeniu/wyłączeniu RGB nie jest zachowany stan czy aktywny jest stały kolor czy animacjia przejścia kolorów

FICZERY:

- Na zablokowanym ekranie sterowanie oświetleniem zewnętrznym
- Możliwość zmiany opcji włączenia/wygaszenia ekranu
- Zmiana ikony wulkanu
- Automatyczna blokada po określonym czasie
- Instrukcje przesuwane z lewej na prawo
- Inny sposób wyłączania PIN niż podanie dwóch pustych pinów

ZAPOMNIANE:

- Dodawanie wydarzeń do kalendarza
- Dodanie ikon w ekranie opcji
- Dodać czarne tło
- Dodanie ikony Ph w ekranie blokady

BAJERY:
- Sortowanie work workCycleCaption po czasie

 */
public class MainFragmentActivity extends FragmentActivity implements IAquariumStateDependent{

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    String initLocale;

    Handler syncWithServerHander = new Handler();
    private Runnable startSyncTaskRunnable = new SyncWithServerRunnable(this, syncWithServerHander);


    private void registerHandler()
    {
        canSync = true;
        syncWithServerHander.postDelayed(startSyncTaskRunnable, 500);
    }

    private void unregisterHandler()
    {
        canSync = false;
        syncWithServerHander.removeCallbacks(startSyncTaskRunnable);
        syncWithServerHander.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        initLocale = StateSingleton.getInstance().getLocale();

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_manual);

        // Create the adapter that will return additionalInfo fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        StateSingleton.getInstance().setCurrentScreenId(getApplicationContext(), StateSingleton.getInstance().getMode() == AquariumState.ActivityType.MANUAL.value ?   AquariumState.ActivityType.MANUAL.value :  AquariumState.ActivityType.AUTO.value  );
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        registerHandler();
        super.onResume();
        refreshBackground();

        if(isNetworkError()){
            interruptWithNetworkError();
        }
    }

    private boolean isNetworkError()
    {
        return !NetworkSingleton.getInstance().isConnected();
    }


    private void interruptWithNetworkError()
    {
        startActivity(new Intent(this, NoConnectionErrorActivity.class));
        finish();
    }

    @Override
    protected void onPause() {
        unregisterHandler();
        super.onPause();
    }

    @Override
    public void recreateFromState()
    {
        if( mSectionsPagerAdapter.mFragmentAtPos0 != null )
            ((IAquariumStateDependent) mSectionsPagerAdapter.mFragmentAtPos0).recreateFromState();
    }

    private boolean canSync = true;
    public boolean canSyncWithServer()
    {
        return canSync;
    }

    @Override
    public void startActivityBasedOnState(Class<?> cls, Bundle extras) {
        Intent i = new Intent(this, cls);
        i.putExtras(extras);
        startActivity(i);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void refreshBackground()
    {
        Picasso.with(getApplicationContext()).load(StateSingleton.getInstance().getBackground()).into(new Target()
        {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                ViewPager pager = findViewById(R.id.pager);
                pager.setBackground(new BitmapDrawable(getApplicationContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                if(errorDrawable != null)
                    Log.d("AQUACASE", errorDrawable.toString());
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null)
                    Log.d("AQUACASE", placeHolderDrawable.toString());
            }
        });

    }

    //UPDATE FRAGMENT ON POSITION 0 AND NOTIFY PAGER
    public void refreshFragments(){

        if( mSectionsPagerAdapter.mFragmentAtPos0 != null ) getSupportFragmentManager().beginTransaction().remove(mSectionsPagerAdapter.mFragmentAtPos0).commit();

        if (StateSingleton.getInstance().getMode() == 0 ) {
            mSectionsPagerAdapter.mFragmentAtPos0 =  AutoFragment.newInstance();
            StateSingleton.getInstance().setCurrentScreenId(getApplicationContext(), AquariumState.ActivityType.AUTO.value );

        }
        else{
            mSectionsPagerAdapter.mFragmentAtPos0 =  ManualFragment.newInstance();
            StateSingleton.getInstance().setCurrentScreenId(getApplicationContext(), AquariumState.ActivityType.MANUAL.value );
        }

        mSectionsPagerAdapter.notifyDataSetChanged();
        getSupportFragmentManager().executePendingTransactions();
        refreshBackground();
    }

    public void refreshSideFragment()
    {
        if( mSectionsPagerAdapter.mFragmentAtPos1 != null ) getSupportFragmentManager().beginTransaction().remove(mSectionsPagerAdapter.mFragmentAtPos1).commit();
        getSupportFragmentManager().executePendingTransactions();
        mSectionsPagerAdapter.mFragmentAtPos1 =  SideFragment.newInstance();
        mSectionsPagerAdapter.forceRecreate = true;
        mSectionsPagerAdapter.notifyDataSetChanged();
        refreshBackground();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns additionalInfo fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public android.support.v4.app.Fragment mFragmentAtPos0;
        public android.support.v4.app.Fragment mFragmentAtPos1;
        public boolean forceRecreate;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            forceRecreate = false;
        }

        @Override
        public Fragment getItem(int position) {
            //FRAGMENT II ALWAYS STAYS THE SAME, ONLY FRAGMENT ON POSITION 0 IS REPLACED
            if( position == 0 ){
                if (mFragmentAtPos0 == null) {
                    if (StateSingleton.getInstance().getMode() == AquariumState.ActivityType.MANUAL.value ) {
                        mFragmentAtPos0 = AutoFragment.newInstance();
                    }
                    else{
                        mFragmentAtPos0 = ManualFragment.newInstance();
                    }
                }
                return mFragmentAtPos0;
            }
            else{
                if (mFragmentAtPos1 == null) {
                    mFragmentAtPos1 = SideFragment.newInstance();
                }
                return  mFragmentAtPos1;
            }
        }

        @Override
         public int getItemPosition(Object object)
        {
            //OVERLOADED TO ALLOW FRAGMENT REPLACEMENT
            if ( (object instanceof ManualFragment && mFragmentAtPos0 instanceof AutoFragment) || (object instanceof AutoFragment && mFragmentAtPos0 instanceof ManualFragment)  )
                return POSITION_NONE;
            else if(object instanceof SideFragment && forceRecreate){
                forceRecreate = false;
                return POSITION_NONE;
            }
            return POSITION_UNCHANGED;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
