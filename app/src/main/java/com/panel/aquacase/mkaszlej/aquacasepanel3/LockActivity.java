package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class LockActivity extends AquaBaseActivity {

    ImageView screenIcon;
    ImageButton lockButton;
    ImageView autoView;
    TextView temperatureLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.LOCKSCREEN.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);
        intializeView();
    }

    @Override
    public void onResume() {
        super.onResume();
        recreateFromState();
    }

    private void intializeView()
    {
        screenIcon = findViewById(R.id.screenIcon);
        lockButton = findViewById(R.id.lockButton);
        autoView = findViewById(R.id.autoView);
        temperatureLabel = findViewById(R.id.temperatureText);

        Util.setNormalDrawable(getApplicationContext(), screenIcon, R.drawable.thermoscreenicon, 256, 256);
        Util.setStateDrawable(getApplicationContext(), lockButton, R.drawable.lockscreen0,  R.drawable.lockscreen1, 512, 512);
    }

    @Override
    public void recreateFromState()
    {
        temperatureLabel.setText(String.format("%d"+(char) 0x00B0+"C", StateSingleton.getInstance().getCurrentTemperature() ));

        lockButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent start = null;
                if (StateSingleton.getInstance().getPinCode() == -1)
                {
                    start = new Intent(LockActivity.this, MainFragmentActivity.class);  //PINCODE NOT SET
                }
                else
                {
                    start = new Intent(LockActivity.this, PinCodeActivity.class);      //PINCODE SET
                }
                startActivity(start);
                return false;
            }
        });

        boolean in_auto_mode =(StateSingleton.getInstance().getMode() == AquariumState.ActivityType.AUTO.value);
        autoView.setImageResource(in_auto_mode ? R.drawable.autobutton1 : R.drawable.manualbtnview1);
    }

}
