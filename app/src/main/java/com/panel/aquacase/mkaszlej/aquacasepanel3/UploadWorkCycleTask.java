package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class UploadWorkCycleTask extends AsyncTask<String, String, String>
{
    private String url_get_state;

    private final String TAG = "AQUACASE";
    private final String PREFIX = "[UploadWorkCycleTask] ";
    private final String TAG_SUCCESS = "success";
    private final String METHOD = "POST";

    WorkCycle work_cycle;

    UploadWorkCycleTask(String url, WorkCycle cycle) {
        super();
        url_get_state = url;
        work_cycle = cycle;
        if (url.length() == 0 || cycle == null) throw new AssertionError();
    }

    private JSONObject makeHttpRequest() {
        if (!NetworkSingleton.getInstance().isConnected()) return null;
        return new JSONParser().makeHttpRequest(url_get_state, METHOD, work_cycle.getData());
    }

    private boolean isRequestOk(JSONObject json)
    {
        if (json != null) {
            if(JSONParser.server_response_code != 200) {
                Log.d(TAG, PREFIX + "HTTP error: " + JSONParser.server_response_code);
                return false;
            }
            Log.d(TAG, PREFIX + "server responded: " + json.toString());
            return true;
        }
        Log.d(TAG, PREFIX + "FAIL! message was null");
        return false;
    }

    private void processServerResponse(JSONObject json) throws JSONException
    {
        if (json.getInt(TAG_SUCCESS) == 1)
        {
            processSuccessMessage();
        }
        else
            processFailureMessage();
    }

    private void processSuccessMessage() {
        StateSingleton.getInstance().setServerLastMessageId(StateSingleton.getInstance().getMessageId());
        NetworkSingleton.getInstance().setIsConnected(true);
        StateSingleton.getInstance().increaseWorkcycleId();
    }

    private void processFailureMessage()
    {
//        NetworkSingleton.getInstance().downloadStateFromServer();//TODO! Start network error info activiy
    }

    protected String doInBackground(String... params)
    {
        try {
            JSONObject json = makeHttpRequest();
            if (isRequestOk(json))
                processServerResponse(json);
        } catch (Exception e) {
            processFailureMessage();
            e.printStackTrace();
        }
        return null;
    }
}