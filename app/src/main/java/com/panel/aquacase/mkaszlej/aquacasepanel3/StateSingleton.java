package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class StateSingleton {

    private static StateSingleton m_Instance = null;
    private AquariumState state;
    public AquariumState getState(){
        return state;
    }

    private StateSingleton(){
        state = new AquariumState();
    }

    public static StateSingleton getInstance(){
        m_Instance = (m_Instance == null) ? new StateSingleton() : m_Instance;
        return m_Instance;
    }

    public void readSharedPreferences(Context c)
    {
        SharedPreferences sharedPref = c.getSharedPreferences( c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        int pinCode = sharedPref.getInt( c.getString(R.string.saved_pin_code) , -1);
        int userId = sharedPref.getInt( c.getString(R.string.saved_user_id) , 0);
        int userCode = sharedPref.getInt( c.getString(R.string.saved_user_code) , 0);
        state.pinCode = pinCode;
        state.userId = userId;
        state.userCode = userCode;
        isPinCheckPassed = (pinCode == -1);
    }

    public static void resetState(Context c) {
        m_Instance = new StateSingleton();
        m_Instance.readSharedPreferences(c);
    }

    public void downloadLastMessageIdFromServer() {
        NetworkSingleton.getInstance().downloadLastServerMessageId();
    }

    public void downloadStateFromServer(AquaBaseActivity activity){
        NetworkSingleton.getInstance().downloadStateFromServer(activity);
        showToast("Downloading server state: " + state.messageId);
    }

    public void downloadWorkCyclesFromServer(){
        NetworkSingleton.getInstance().downloadCyclesFromServer();
        showToast("Downloading workcycle data");
    }

    public void downloadCalendarEventsFromServer(){
        NetworkSingleton.getInstance().downloadCalendarEventsFromServer();
        showToast("Downloading calendar events data");
    }

    public void sendWorkCycle(WorkCycle w ){
        NetworkSingleton.getInstance().sendWorkCycleToServer(w);
        showToast("Uploading workcycle data");
    }

    public void sendCalendarEvent(CalendarEvent c){
        NetworkSingleton.getInstance().sendCalendarEventToServer(c);
        showToast("Uploading calendar event data");
    }

    public void sendStateToServer(){
        NetworkSingleton.getInstance().sendStateToServer();
        showToast("Uploading state to server: " + state.messageId);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Pin blockade
    private boolean isPinCheckPassed = false;
    public boolean IsPinCheckRequired(){
        return !isPinCheckPassed;
    }
    public void PinCodeEntered() { isPinCheckPassed = true; }
    public void setPinCode( Context c, int newValue ){
        state.pinCode = newValue;
        sendStateToServer();
        saveNewPinInSharedPreferences(c,newValue);
    }
    private void saveNewPinInSharedPreferences(Context c, int newValue) {
        SharedPreferences sharedPref = c.getSharedPreferences(c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt( c.getString(R.string.saved_pin_code), newValue);
        editor.commit();
    }
    ///////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Last downloaded WorkcycleId
    private int server_last_workcycle_id = -1;
    public void setServerLastWorkcycleId(int id){ server_last_workcycle_id = id; }
    public int getServerLastWorkcycleId() {return  server_last_workcycle_id; }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Last downloaded calendarEventId
    private int server_last_calendar_event_id = -1;
    public void setServerLastCalendarEventId(int id){ server_last_calendar_event_id = id; }
    public int getServerLastCalendarEventId() {return  server_last_calendar_event_id; }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Last server message id
    private int server_last_message_id = -1;
    public void setServerLastMessageId(int id) {
        server_last_message_id = id;
    }
    public int getServerLastMessageId() {
        return server_last_message_id;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Screen Id update
    private Context current_context = null;
    private int local_currently_displayed_screen_id = -1;
    private boolean is_auto_switching_screen = false;

    public int getCurrentlyDisplayedScreenId() { return local_currently_displayed_screen_id; }

    public void setLocalCurrentlyDisplayedScreenId(int screenId) {
        local_currently_displayed_screen_id = screenId;
    }

    public void setCurrentScreenId(Context c, int newValue) {
        current_context = c;
        updateServerScreenId(newValue);
        setLocalCurrentlyDisplayedScreenId(newValue);
        is_auto_switching_screen = false;
    }

    private void updateServerScreenId(int new_screen_id) {
        if(shouldSendScreenIdToServer(new_screen_id)) {
            state.currentScreenId = new_screen_id;
            sendStateToServer();
        }
    }

    private boolean shouldSendScreenIdToServer(int new_screen_id) {
        return !isAutoSwitchingScreen() && isScreenIdTrackedByServer(new_screen_id) && getCurrentlyDisplayedScreenId() != new_screen_id;
    }

    private  boolean isScreenIdTrackedByServer(int screen_id){
        return !(
                screen_id == AquariumState.ActivityType.ANIMATIONSHOW.value ||
                screen_id == AquariumState.ActivityType.ANIMATIONSELECT.value ||
                screen_id == AquariumState.ActivityType.CALENDARADD.value ||
                screen_id == AquariumState.ActivityType.COLOR.value ||
                screen_id == AquariumState.ActivityType.PIN.value ||
                screen_id == AquariumState.ActivityType.TIMESELECT.value ||
                screen_id == AquariumState.ActivityType.CODE.value ||
                screen_id == AquariumState.ActivityType.NO_CONNECTION.value ||
                screen_id == AquariumState.ActivityType.CONNECT.value
        );
    }
    public int getServerCurrentScreenId(){
        return state.currentScreenId;
    }
    public void setIsAutoSwitchingScreen() {is_auto_switching_screen = true;}
    public boolean isAutoSwitchingScreen() {return  is_auto_switching_screen;}

    ///////////////////////////////////////////////////////////////////////////////////////////

    //////////////
    /*-- SETTERS --*/
    //////////////

    public void setMode(int newValue){
        state.mode = newValue;
        sendStateToServer();
    }

    public void setRgbPower(int newValue){
        state.rgbPower = newValue;
        sendStateToServer();
    }

    public void setLightPower(int newValue){
        state.lightPower = newValue;
        sendStateToServer();
    }

    public void setOutRgbPower(int newValue){
        state.outRgbPower = newValue;
        sendStateToServer();
    }

    public void setRgbColor(  Context c, int newValue ){
        state.rgbColorId = newValue;
        sendStateToServer();
    }

    public void setOutRgbColor(int newValue){
        state.outRgbColor = newValue;
        sendStateToServer();
    }

    public void setRgbAnimationType(int newValue){
        state.rgbAnimationType = newValue;
        sendStateToServer();
    }

    public void setOutRgbAnimtionType(int newValue){
        state.outRgbAnimationType = newValue;
        sendStateToServer();
    }

    public void setRgbActive(int newValue){
        state.rgbActive = newValue;
        sendStateToServer();
    }

    public void setOutRgbActive(int newValue){
        state.outRgbActive = newValue;
        sendStateToServer();
    }

    public void setLightActive(int newValue){
        state.lightActive = newValue;
        sendStateToServer();
    }

    public void setO2Active(int newValue){
        state.o2Active = newValue;
        sendStateToServer();
    }

    public void setCO2Active(int newValue){
        state.co2Active = newValue;
        sendStateToServer();
    }

    public void setFoodActive(int newValue){
        state.foodActive = newValue;
        sendStateToServer();
    }

    public void setVolcanoActive(int newValue){
        state.volcanoActive = newValue;
        sendStateToServer();
    }

    public void setLFilterActive(int newValue){
        state.leftPumpActive = newValue;
        sendStateToServer();
    }

    public void setRFilterActive(int newValue){
        state.rightPumpActive = newValue;
        sendStateToServer();
    }

    public void setDesiredTemperature(int newValue){
        state.desiredTemperature = newValue;
        sendStateToServer();
    }

    public void setDisplayOffTime(int newValue){
        state.dispTimeOff = newValue;
        sendStateToServer();
    }

    public void setDisplayLockTime(int newValue){
        state.dispTimeLock = newValue;
        sendStateToServer();
    }

    public void setCurrentScreenType(int newValue){
        state.currentScreenType = newValue;
        sendStateToServer();
    }

    public void increaseWorkcycleId(){
        state.increaseWorkcycleId();
        setServerLastWorkcycleId(state.workcycleId);
        sendStateToServer();
    }

    public void increaseCalendarEventId(){
        state.increaseWorkcycleId();
        setServerLastCalendarEventId(state.calendarEventId);
        sendStateToServer();
    }

    /////////////

    public void toggleBackground(){
        state.bgResourceId = (state.bgResourceId < 7) ? state.bgResourceId + 1 : 0;
        sendStateToServer();
    }

    public int getBackground() {
        if(state.bgResourceId == 0)
            return R.drawable.background1;
        else if( state.bgResourceId == 1)
            return R.drawable.background2;
        else if( state.bgResourceId == 2)
            return R.drawable.background3;
        else if( state.bgResourceId == 3)
            return R.drawable.background4;
        else if( state.bgResourceId == 4)
            return R.drawable.background5;
        else if( state.bgResourceId == 5)
            return R.drawable.backgroundanim;
        else if( state.bgResourceId == 6)
            return R.color.caldroid_555;
        return R.color.caldroid_black;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    // Locale
    public void toggleLocale() {
        state.languageCode = (state.languageCode == 0) ? state.languageCode + 1 : 0;
        sendStateToServer();
    }
    public String getLocale() {
        return (state.languageCode  == 1) ? "en" : "pl";
    }
    /////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////
    // MessageId
    public int getAndIncrementMessageId(){
        state.messageId++;
        return state.messageId;
    }
    public int getMessageId(){
        return state.messageId;
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    public int getPinCode(){
        return state.pinCode;
    }
    public int getMode(){
        return state.mode;
    }
    public int getRgbPower(){
        return state.rgbPower;
    }
    public int getLightPower(){
        return state.lightPower;
    }
    public int getOutRgbPower(){
        return state.outRgbPower;
    }
    public int getRgbColorId(){ return state.rgbColorId; }
    public int getOutRgbColor(){
        return state.outRgbColor;
    }
    public int getRgbAnimationType(){
        return state.rgbAnimationType;
    }
    public int getOutRgbAnimationType(){
        return state.outRgbAnimationType;
    }
    public int isRgbActive() {
        return state.rgbActive;
    }
    public int isOutRgbActive(){
        return state.outRgbActive ;
    }
    public boolean isLightActive(){
        return state.lightActive > 0;
    }
    public boolean isO2Active(){
        return state.o2Active > 0;
    }
    public boolean isCO2Active(){
        return state.co2Active > 0;
    }
    public boolean isFoodActive(){
        return state.foodActive > 0;
    }
    public boolean isVolcanoActive(){
        return state.volcanoActive > 0;
    }
    public boolean isLFilterActive(){
        return state.leftPumpActive > 0;
    }
    public boolean isRFilterActive(){
        return state.rightPumpActive > 0;
    }
    public int getCurrentTemperature(){
        return state.currentTemperature;
    }
    public int getDesiredTemperature(){
        return state.desiredTemperature;
    }
    public int getDisplayOffTime(){
        return state.dispTimeOff;
    }
    public int getDisplayLockTime(){
        return state.dispTimeLock;
    }
    public int getCurrentScreenType(){
        return state.currentScreenType;
    }
    public int getWorkcycleId(){ return state.workcycleId; }
    public int getCalendarEventId(){ return state.workcycleId; }

    ////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////
    // CALENDAR
    public void addEvent(int day, int month, int year, String title, boolean isAlarmSet, int aH, int aM){
        CalendarEvent c = new CalendarEvent(day,month,year,title);
        if(isAlarmSet) c.setAlarm(aH,aM);
        if(state.events.contains(c)){
            return; //NOT ALLOW 2 EVENTS SAME TITLE SAME DAY SAME MONTH SAME YEAR
        }
        state.events.add( c );
        sendCalendarEvent(c);
    }
    public void editEvent(int day, int month, int year, String title, String newTitle, boolean isAlarmSet, int aH, int aM){
        for( CalendarEvent event : state.events ){
            if( event.getDay() == day && event.getMonth() == month && event.getYear() == year && event.getTitle().equals(title)){
                if(isAlarmSet) event.setAlarm(aH , aM);
                else event.unsetAlarm();

                event.setTitle(newTitle);

                sendCalendarEvent(event);

                return;
            }
        }
        Log.d("COMM", "COULDNT FIND EVENT " + day + "-" + month + "-" + year + "-" + title);
    }
    public void removeEvent(int day, int month, int year, String title){
        for( CalendarEvent event : state.events ){
            if( event.getDay() == day && event.getMonth() == month && event.getYear() == year && event.getTitle().equals(title)){
                state.events.remove(event);
                Log.d("COMM", "EVENT REMOVED");
                sendStateToServer();
                return;
            }
        }
        Log.d("COMM", "COULDNT FIND EVENT "+day+"-"+month+"-"+year+"-"+title);
    }
    public ArrayList<CalendarEvent> getEvents(){
        return state.events;
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////
    // Work cycles
    public ArrayList<WorkCycle> getWorkCycle(int type){
        ArrayList<WorkCycle> a = new ArrayList<WorkCycle>();
        for( WorkCycle w : state.workCycles ){
            if( w.getType() == type ){
                a.add(w);
            }
        }
        return a;
    }
    public void updateWorkCycle(int type, int id, int hStart, int mStart, int hEnd, int mEnd, int isSet){
        for( WorkCycle w : state.workCycles ) {
            if(w.getType() == type && w.getId() == id){
                w.update(hStart, mStart, hEnd, mEnd, isSet);
                sendWorkCycle(w);
                return;
            }
        }
    }
    public void activateCycle(int type, int id){
        for( WorkCycle w : state.workCycles ) {
            if(w.getType() == type && w.getId() == id){
                w.setActive(1);
                sendWorkCycle(w);
                return;
            }
        }
    }
    public void deactivateCycle(int type, int id){
        for( WorkCycle w : state.workCycles ) {
            if(w.getType() == type && w.getId() == id){
                w.setActive(0);
                sendWorkCycle(w);
                return;
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////
    // User id and user code and serial
    public int getUserId(){
        return state.userId;
    }
    public void setSerialNumber( Context c ,int id ){
        setUserId(c, id);
    }
    public void setUserId( Context c,int id ) {
        this.state.userId = id;
        SharedPreferences sharedPref = c.getSharedPreferences(c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt( c.getString(R.string.saved_user_id), id);
        editor.commit();
    }
    public int getUserCode(){
        return state.userCode;
    }
    public void setUserCode( Context c,  int code ){
        this.state.userCode = code;
        SharedPreferences sharedPref = c.getSharedPreferences(c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt( c.getString(R.string.saved_user_code), code);
        editor.commit();
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    private void showToast(String text){
        if (current_context != null) {
            Toast.makeText(current_context, text, Toast.LENGTH_SHORT).show();
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    public Class<?> getActivityClassFromScreenId(int screenId)
    {
        if(screenId == AquariumState.ActivityType.LOCKSCREEN.value)
        {
            return (LockActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.MANUAL.value)
        {
            return (MainFragmentActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.AUTO.value)
        {
            return (MainFragmentActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.SIDE.value)
        {
            return (MainFragmentActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.LIGHT.value)
        {
            return (LightActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.RGB.value)
        {
            return (RgbActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.CALENDAR.value)
        {
            return (CalendarActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.DISPLAY.value)
        {
            return (DisplayActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.SCHEDULE.value)
        {
            return (ScheduleActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.THERMO.value)
        {
            return (ThermoActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.AQUARIUMMANUAL.value)
        {
            return (WebViewActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.GUARANTEEMANUAL.value)
        {
            return (WebViewActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.PANELMANUAL.value)
        {
            return (WebViewActivity.class);
        }
        else if(screenId == AquariumState.ActivityType.FACTORYRESET.value)
        {
            return (WebViewActivity.class);
        }
        return (MainFragmentActivity.class);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    public Bundle getActivityStartBundle(int screenId)
    {
        Bundle bundle = new Bundle();
        if(screenId == AquariumState.ActivityType.SCHEDULE.value)
        {
            bundle.putInt("TYPE", StateSingleton.getInstance().getCurrentScreenType());
        }
        else if(screenId == AquariumState.ActivityType.AQUARIUMMANUAL.value)
        {
            bundle.putInt("manualtype", screenId);
        }
        else if(screenId == AquariumState.ActivityType.GUARANTEEMANUAL.value)
        {
            bundle.putInt("manualtype", screenId);
        }
        else if(screenId == AquariumState.ActivityType.PANELMANUAL.value)
        {
            bundle.putInt("manualtype", screenId);
        }
        else if(screenId == AquariumState.ActivityType.FACTORYRESET.value)
        {
            bundle.putInt("manualtype", screenId);
        }
        return bundle;
    }

    public boolean shouldRefreshScreen(int screen_id)
    {
        return !(
                screen_id == AquariumState.ActivityType.ANIMATIONSHOW.value ||
                        screen_id == AquariumState.ActivityType.ANIMATIONSELECT.value ||
                        screen_id == AquariumState.ActivityType.CALENDARADD.value ||
                        screen_id == AquariumState.ActivityType.COLOR.value ||
                        screen_id == AquariumState.ActivityType.PIN.value ||
                        screen_id == AquariumState.ActivityType.SIDE.value ||
                        screen_id == AquariumState.ActivityType.TIMESELECT.value ||
                        screen_id == AquariumState.ActivityType.WEB.value ||
                        screen_id == AquariumState.ActivityType.AQUARIUMMANUAL.value ||
                        screen_id == AquariumState.ActivityType.PANELMANUAL.value ||
                        screen_id == AquariumState.ActivityType.GUARANTEEMANUAL.value ||
                        screen_id == AquariumState.ActivityType.FACTORYRESET.value ||
                        screen_id == AquariumState.ActivityType.CODE.value ||
                        screen_id == AquariumState.ActivityType.NO_CONNECTION.value ||
                        screen_id == AquariumState.ActivityType.CONNECT.value
        );

    }

    public boolean shouldShowNewScreen()
    {
        boolean screen_has_changed = (getCurrentlyDisplayedScreenId() != getServerCurrentScreenId());
        return  screen_has_changed && shouldChangeToScreen(getCurrentlyDisplayedScreenId(), getServerCurrentScreenId());
    }

    public boolean shouldChangeToScreen(int from_screen_id, int to_screen_id)
    {
        return canLeaveScreen(from_screen_id) && canAutoShowScreen(to_screen_id);
    }

    public boolean canLeaveScreen(int screen_id)
    {
        return !(
                screen_id == AquariumState.ActivityType.ANIMATIONSHOW.value ||
                        screen_id == AquariumState.ActivityType.PIN.value ||
                        screen_id == AquariumState.ActivityType.COLOR.value ||
                        screen_id == AquariumState.ActivityType.TIMESELECT.value ||
                        screen_id == AquariumState.ActivityType.CALENDARADD.value ||
                        screen_id == AquariumState.ActivityType.ANIMATIONSELECT.value ||
                        screen_id == AquariumState.ActivityType.CODE.value ||
                        screen_id == AquariumState.ActivityType.NO_CONNECTION.value ||
                        screen_id == AquariumState.ActivityType.CONNECT.value
        );
    }

    public boolean canAutoShowScreen(int screen_id)
    {
        return !(
                screen_id == AquariumState.ActivityType.ANIMATIONSHOW.value ||
                        screen_id == AquariumState.ActivityType.ANIMATIONSELECT.value ||
                        screen_id == AquariumState.ActivityType.CALENDARADD.value ||
                        screen_id == AquariumState.ActivityType.COLOR.value ||
                        screen_id == AquariumState.ActivityType.PIN.value ||
                        screen_id == AquariumState.ActivityType.SIDE.value ||
                        screen_id == AquariumState.ActivityType.TIMESELECT.value ||
                        screen_id == AquariumState.ActivityType.NO_CONNECTION.value ||
                        screen_id == AquariumState.ActivityType.CONNECT.value
        );
    }

    public boolean shouldRefreshCurrentScreen()
    {
        boolean screen_has_changed = (getCurrentlyDisplayedScreenId() != getServerCurrentScreenId());
        return  !screen_has_changed && shouldRefreshScreen(getCurrentlyDisplayedScreenId());
    }
}