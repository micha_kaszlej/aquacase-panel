package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

public class ManualFragment extends android.support.v4.app.Fragment implements IAquariumStateDependent
{
    ImageView lightButton;
    ImageView rgbButton;
    ImageView o2Button;
    ImageView volcanoButton;
    ImageView co2Button;
    ImageView foodButton;
    ImageView lockButton;
    ImageView calButton;
    ImageView autoButton;
    ImageView thermoButton;
    ImageView lPumpButton;
    ImageView rPumpButton;

    public static ManualFragment newInstance()
    {
        ManualFragment fragment = new ManualFragment();
        return fragment;
    }

    // Required empty public constructor
    public ManualFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_manual, container, false);
        intializeView(v);
        recreateFromState();
        return v;
    }

    public void onResume()
    {
        super.onResume();
    }


    private void updatePumpButtonState(ImageView btn, boolean should_be_selected, int animated_drawable, int static_drawable, boolean left_to_right_rotation)
    {
        boolean is_selected = btn.isSelected();
        if (should_be_selected)
        {
            if(!is_selected || btn.getAnimation() == null || !btn.getAnimation().isInitialized())
            {
                // ENABLE PUMP
                btn.setImageResource(animated_drawable);

                if (btn.getMeasuredHeight() != 0 && btn.getMeasuredWidth() != 0) {
                    Animation rotate = new RotateAnimation
                    (
                        left_to_right_rotation ? 360.0f : 0.0f,
                        left_to_right_rotation ? 0.0f : 360.0f,
                        Animation.ABSOLUTE,
                        btn.getMeasuredWidth()/2.0f,
                        Animation.ABSOLUTE,
                        btn.getMeasuredHeight()/2.0f
                    );
                    rotate.setRepeatCount(-1);
                    rotate.setDuration(1000);

                    btn.setAnimation(rotate);
                    btn.startAnimation(rotate);
                }
            }
            btn.setSelected(true);
        }
        else
        {
            btn.setSelected(false);
            // DISABLE PUMP
            if(is_selected)
            {
                btn.setImageResource(static_drawable);
                btn.clearAnimation();
            }
        }
    }

    private void updateAnimButtonState(ImageView btn, boolean should_be_selected, int animated_drawable, int static_drawable)
    {
        boolean is_selected = btn.isSelected();
        if(should_be_selected)
        {
            if(!is_selected)
            {
                btn.setSelected(true);
                AnimationDrawable ad = (AnimationDrawable) getResources().getDrawable(animated_drawable);
                btn.setImageDrawable(ad);
                ad.start();
            }
        }
        else
        {
            if(is_selected)
            {
                btn.setSelected(false);
                btn.setImageResource(static_drawable);
            }
        }
    }

    @Override
    public void recreateFromState()
    {
        if (isAdded()) {
            StateSingleton s = StateSingleton.getInstance();
            lightButton.setSelected(s.isLightActive());
            rgbButton.setSelected(s.isRgbActive() > 0);
            updateAnimButtonState(o2Button, s.isO2Active(), R.drawable.o2, R.drawable.o20);
            updateAnimButtonState(volcanoButton, s.isVolcanoActive(), R.drawable.volcano, R.drawable.volcano0);
            updateAnimButtonState(co2Button, s.isCO2Active(), R.drawable.co2, R.drawable.co20);
            updatePumpButtonState(lPumpButton, s.isLFilterActive(), R.drawable.pump1, R.drawable.pump0, true);
            updatePumpButtonState(rPumpButton, s.isRFilterActive(), R.drawable.pumpr1, R.drawable.pumpr0, false);
            updateAnimButtonState(foodButton, s.isFoodActive(), R.drawable.food, R.drawable.food0);
        }
    }

    @Override
    public void startActivityBasedOnState(Class<?> cls, Bundle extras) {
        // DO nothing as mainfragmentactivity handles this
    }
    @Override
    public boolean canSyncWithServer() {
        return false;
    }

    public void intializeView(View u)
    {
        // We have to delay setting pump animations to get proper dimensions
        u.getViewTreeObserver().addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() {
            @Override
            public void onWindowFocusChanged(final boolean hasFocus) {
                updatePumpButtonState(lPumpButton, StateSingleton.getInstance().isLFilterActive(), R.drawable.pump1, R.drawable.pump0, true);
                updatePumpButtonState(rPumpButton, StateSingleton.getInstance().isRFilterActive(), R.drawable.pumpr1, R.drawable.pumpr0, false);
            }
        });

        lightButton = u.findViewById(R.id.lightButton);

        rgbButton = u.findViewById(R.id.rgbButton);

        o2Button = u.findViewById(R.id.o2Button);
        o2Button.setImageResource(R.drawable.o20);

        volcanoButton = u.findViewById(R.id.volcanoButton);
        volcanoButton.setImageResource(R.drawable.volcano0);

        co2Button = u.findViewById(R.id.co2Button);
        co2Button.setImageResource(R.drawable.co20);

        foodButton = u.findViewById(R.id.foodButton);
        foodButton.setImageResource(R.drawable.food0);

        lPumpButton = u.findViewById(R.id.pumpButton);
        rPumpButton = u.findViewById(R.id.pumpRButton);

        lockButton = u.findViewById(R.id.lockButton);
        calButton = u.findViewById(R.id.calButton);
        autoButton = u.findViewById(R.id.autoButton);

        thermoButton = u.findViewById(R.id.thermoButton);

        //Set on click listener
        lightButton.setOnClickListener(onClickListener);
        rgbButton.setOnClickListener( onClickListener );
        foodButton.setOnClickListener( onClickListener );
        thermoButton.setOnClickListener( onClickListener );
        o2Button.setOnClickListener( onClickListener );
        co2Button.setOnClickListener( onClickListener );
        lPumpButton.setOnClickListener( onClickListener );
        rPumpButton.setOnClickListener( onClickListener );
        lockButton.setOnClickListener( onClickListener );
        volcanoButton.setOnClickListener( onClickListener );
        calButton.setOnClickListener( onClickListener );
        autoButton.setOnClickListener( onClickListener );

        //LOAD IMAGES WITH PICASSO DUE TO MEMORY ERRORS
        Context context = this.getActivity();
        Util.setStateDrawable(context, lightButton, R.drawable.light0, R.drawable.light2);
        Util.setStateDrawable(context, rgbButton, R.drawable.rgb0, R.drawable.rgb2);
        Util.setNormalDrawable(context, thermoButton, R.drawable.thermo0);
        Util.setStateDrawable(context, autoButton, R.drawable.auto0, R.drawable.auto1);
        Util.setNormalDrawable(context, calButton, R.drawable.cal0);
        Util.setNormalDrawable(context, lockButton, R.drawable.lock0);

        //SET SCHEDULE SCREEN LINKS
        // Set on long click listeners
        lightButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.LIGHT.value);
                startActivity(i);
                return true;
            }
        });

        rgbButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.RGB.value);
                startActivity(i);
                return true;
            }
        });

        foodButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.FEED.value);
                startActivity(i);
                return true;
            }
        });

        o2Button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.O2.value);
                startActivity(i);
                return true;
            }
        });

        co2Button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.CO2.value);
                startActivity(i);
                return true;
            }
        });

        volcanoButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.VOLCANO.value);
                startActivity(i);
                return true;
            }
        });

        lPumpButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.LPUMP.value);
                startActivity(i);
                return true;
            }
        });

        rPumpButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ScheduleActivity.class);
                i.putExtra("TYPE", AquariumState.ActivityType.RPUMP.value);
                startActivity(i);
                return true;
            }
        });

        //NO SCHEDULE FOR BELOW ITEMS, BUT LONG PRESS SHOULD STILL WORK AS USUAL TAP
        lockButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        calButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        autoButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }

    public void lightButtonAction( View v ){
        startActivity(new Intent(getActivity(), LightActivity.class));
    }

    public void thermoButtonAction( View v ){
        startActivity(new Intent(getActivity(), ThermoActivity.class));
    }

    public void rgbButtonAction( View v ){
        startActivity(new Intent(getActivity(), RgbActivity.class));
    }

    public void calButtonAction( View v ){
        startActivity(new Intent(getActivity(), CalendarActivity.class));
    }

    public void autoAction( View v ){
        StateSingleton.getInstance().setMode(AquariumState.ActivityType.MANUAL.value );
        ((MainFragmentActivity) getActivity()).refreshFragments();

        StateSingleton.getInstance().setCurrentScreenId(getActivity(), AquariumState.ActivityType.MANUAL.value);
    }

    public void lockButtonAction( View v ){
        startActivity(new Intent(getActivity(), LockActivity.class));
    }

    public void leftPumpButtonAction(View v )
    {
        boolean is_selected = lPumpButton.isSelected();
        updatePumpButtonState(lPumpButton, !is_selected, R.drawable.pump1, R.drawable.pump0, true);
        StateSingleton.getInstance().setLFilterActive(is_selected ? 0 : 1);
    }

    public void rightPumpButtonAction(View v )
    {
        boolean is_selected = rPumpButton.isSelected();
        updatePumpButtonState(rPumpButton, !is_selected, R.drawable.pumpr1, R.drawable.pumpr0, false);
        StateSingleton.getInstance().setRFilterActive(is_selected ? 0 : 1);
    }

    public void o2ButtonAction( View v )
    {
        updateAnimButtonState(o2Button,!o2Button.isSelected(),R.drawable.o2,R.drawable.o20);
        StateSingleton.getInstance().setO2Active(o2Button.isSelected() ? 1 : 0);
    }

    public void volcanoButtonAction( View v )
    {
        updateAnimButtonState(volcanoButton,!volcanoButton.isSelected(),R.drawable.volcano,R.drawable.volcano0);
        StateSingleton.getInstance().setVolcanoActive(volcanoButton.isSelected() ? 1 : 0);
    }

    public void co2ButtonAction( View v )
    {
        updateAnimButtonState(co2Button,!co2Button.isSelected(),R.drawable.co2,R.drawable.co20);
        StateSingleton.getInstance().setCO2Active(co2Button.isSelected() ? 1 : 0);
    }

    public void foodButtonAction( View v )
    {
        updateAnimButtonState(foodButton,!foodButton.isSelected(),R.drawable.food,R.drawable.food0);
        StateSingleton.getInstance().setFoodActive(foodButton.isSelected() ? 1 : 0);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            switch(v.getId()){
                case R.id.lightButton:
                    lightButtonAction( v );
                    break;
                case R.id.rgbButton:
                    rgbButtonAction( v );
                    break;
                case R.id.thermoButton:
                    thermoButtonAction( v );
                    break;
                case R.id.pumpRButton:
                    rightPumpButtonAction( v );
                    break;
                case R.id.autoButton:
                    autoAction( v );
                    break;
                case R.id.pumpButton:
                    leftPumpButtonAction( v );
                    break;
                case R.id.foodButton:
                    foodButtonAction( v );
                    break;
                case R.id.o2Button:
                    o2ButtonAction( v );
                    break;
                case R.id.co2Button:
                    co2ButtonAction( v );
                    break;
                case R.id.volcanoButton:
                    volcanoButtonAction(v);
                    break;
                case R.id.calButton:
                    calButtonAction( v );
                    break;
                case R.id.lockButton:
                    lockButtonAction( v );
                    break;
            }
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }
}
