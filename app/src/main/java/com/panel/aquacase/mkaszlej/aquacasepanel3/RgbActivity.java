    package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class RgbActivity extends AquaBaseActivity {

    ImageView screenIcon;
    ImageView toggleLight;
    ImageView toggleOutLight;

    Button animationButton;
    Button outAnimationButton;
    Button colorButton;
    Button outColorButton;

    SeekBar lumosityBar;
    SeekBar outLumosityBar;

    TextView buttonLabel;
    TextView outButtonLabel;
    TextView seekLabel;
    TextView outSeekLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activity_type = AquariumState.ActivityType.RGB.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rgb);
        initializeView();
    }

    @Override
    public void onResume(){
        super.onResume();
        recreateFromState();
    }

    private void initializeView()
    {
        screenIcon = findViewById(R.id.screenIcon);
        toggleLight = findViewById(R.id.toggleLight);
        toggleOutLight = findViewById(R.id.toggleOutLight);

        animationButton = findViewById(R.id.animationButton);
        outAnimationButton = findViewById(R.id.outAnimationButton);
        colorButton = findViewById(R.id.colorButton);
        outColorButton = findViewById(R.id.outColorButton);

        lumosityBar = findViewById(R.id.lumosityBar);
        outLumosityBar = findViewById(R.id.outLumosityBar);

        buttonLabel = findViewById(R.id.enableLabel);
        outButtonLabel = findViewById(R.id.outEnableLabel);
        seekLabel = findViewById(R.id.lumosityDesc);
        outSeekLabel = findViewById(R.id.outLumosityDesc);

        animationButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                StateSingleton.getInstance().setRgbActive(2);
                Intent i = new Intent(RgbActivity.this, AnimationSelectActivity.class);
                startActivity(i);
                return false;
            }
        });

        outAnimationButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                StateSingleton.getInstance().setOutRgbActive(2);
                Intent i = new Intent(RgbActivity.this, AnimationSelectActivity.class);
                i.putExtra("mode",1);
                startActivity(i);
                return false;
            }
        });

        colorButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                StateSingleton.getInstance().setRgbActive(1);
                Intent i = new Intent(RgbActivity.this, ColorActivity.class);
                startActivity(i);
                return false;
            }
        });

        outColorButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                StateSingleton.getInstance().setOutRgbActive(1);
                Intent i = new Intent(RgbActivity.this, ColorActivity.class);
                i.putExtra("mode",1);
                startActivity(i);
                return false;
            }
        });

        lumosityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                StateSingleton.getInstance().setRgbPower(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }
        });

        outLumosityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                StateSingleton.getInstance().setOutRgbPower(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }
        });

        Util.setStateDrawable(getApplicationContext(), screenIcon, R.drawable.rgbscreenicon0,  R.drawable.rgbscreenicon1 );
        Util.setStateDrawable(getApplicationContext(), toggleLight, R.drawable.rgbscreenicon0, R.drawable.rgbscreenicon1, 256, 256 );
        Util.setStateDrawable(getApplicationContext(), toggleOutLight, R.drawable.outrgb0,  R.drawable.outrgb1, 256, 256 );
    }

    @Override
    public void recreateFromState()
    {
        int rgbActive = StateSingleton.getInstance().isRgbActive();
        int outRgbActive = StateSingleton.getInstance().isOutRgbActive();

        toggleLight.setSelected(rgbActive > 0);
        toggleOutLight.setSelected(outRgbActive > 0);

        animationButton.setText(getAnimationButtonText(StateSingleton.getInstance().getRgbAnimationType()));
        outAnimationButton.setText(getAnimationButtonText(StateSingleton.getInstance().getOutRgbAnimationType()));

        lumosityBar.setProgress(StateSingleton.getInstance().getRgbPower());
        outLumosityBar.setProgress(StateSingleton.getInstance().getOutRgbPower());

        if (rgbActive == 0)
        {
            if(screenIcon != null) screenIcon.setSelected(false);
            buttonLabel.setText(R.string.enable);
            lumosityBar.setVisibility(View.GONE);
            colorButton.setVisibility(View.GONE);
            animationButton.setVisibility(View.GONE);
            seekLabel.setVisibility(View.GONE);
        }
        else
        {
            if(screenIcon != null) screenIcon.setSelected(true);
            buttonLabel.setText(R.string.disable);
            lumosityBar.setVisibility(View.VISIBLE);
            colorButton.setVisibility(View.VISIBLE);
            animationButton.setVisibility(View.VISIBLE);
            seekLabel.setVisibility(View.VISIBLE);

            colorButton.setTextColor(getColorFromId(StateSingleton.getInstance().getRgbColorId()));
            colorButton.getBackground().setColorFilter(getColorFromId(StateSingleton.getInstance().getRgbColorId()), PorterDuff.Mode.MULTIPLY);

            if (rgbActive == 1)
            {
                animationButton.setSelected(false);
                colorButton.setSelected(true);
                colorButton.setTextColor(getResources().getColor(R.color.caldroid_black));
            }
            else if (rgbActive == 2)
            {
                animationButton.setSelected(true);
                colorButton.setSelected(false);
            }
        }

        if (outRgbActive == 0)
        {
            outButtonLabel.setText(R.string.enable);
            outLumosityBar.setVisibility(View.GONE);
            outSeekLabel.setVisibility(View.GONE);
            outColorButton.setVisibility(View.GONE);
            outAnimationButton.setVisibility(View.GONE);
            outSeekLabel.setVisibility(View.GONE);
        }
        else
        {
            outButtonLabel.setText(R.string.disable);
            outLumosityBar.setVisibility(View.VISIBLE);
            outSeekLabel.setVisibility(View.VISIBLE);
            outColorButton.setVisibility( View.VISIBLE );
            outAnimationButton.setVisibility(View.VISIBLE);

            outColorButton.setTextColor(getColorFromId(StateSingleton.getInstance().getOutRgbColor()));
            outColorButton.getBackground().setColorFilter(getColorFromId(StateSingleton.getInstance().getOutRgbColor()), PorterDuff.Mode.MULTIPLY);

            if (outRgbActive == 1)
            {
                outAnimationButton.setSelected(false);
                outColorButton.setSelected(true);
                outColorButton.setTextColor(getResources().getColor(R.color.caldroid_black));
            }
            else if (outRgbActive == 2)
            {
                outAnimationButton.setSelected(true);
                outColorButton.setSelected(false);
            }
        }
    }


    private int getColorFromId(int id){
        int[] rainbow = getResources().getIntArray(R.array.rainbow);
        if (rainbow.length == 0) throw new AssertionError();
        for (int i = 0; i < 20; i++)
            if (i == id) return rainbow[i];
        return id;
    }

    public void rgbEnableButton(View v) {
        StateSingleton.getInstance().setRgbActive(v.isSelected() ? 0 : 1);
        recreateFromState();
    }

    public void outRgbEnableButton(View v)
    {
        StateSingleton.getInstance().setOutRgbActive(v.isSelected() ? 0 : 1 );
        recreateFromState();
    }

    public void backAction(View v) {
        startActivity(new Intent(this, MainFragmentActivity.class));
        finish();
    }

    public void colorAction(View v)
    {
        StateSingleton.getInstance().setRgbActive(v.isSelected() ? 2 : 1);
        recreateFromState();
    }

    public void outColorAction(View v)
    {
        StateSingleton.getInstance().setOutRgbActive(v.isSelected() ? 2 : 1);
        recreateFromState();
    }

    public void animationAction(View v)
    {
        StateSingleton.getInstance().setRgbActive(v.isSelected() ? 1 : 2);
        recreateFromState();
    }

    public void outAnimationAction(View v)
    {
        StateSingleton.getInstance().setOutRgbActive(v.isSelected() ? 1 : 2);
        recreateFromState();
    }

    private int getAnimationButtonText( int type ){
        switch (type){
            case 0:
                return R.string.animationBaseFade;
            case 1:
                return R.string.animationAllFade;
            case 2:
                return R.string.animationBaseJump;
            case 3:
                return R.string.animationAllJump;
            case 4:
            default:
                return R.string.animationAuto;
        }
    }
}
