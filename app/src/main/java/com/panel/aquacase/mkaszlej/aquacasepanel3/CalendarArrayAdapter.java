package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class CalendarArrayAdapter extends ArrayAdapter<CalendarEvent>
{
    private final Context context;
    private ArrayList<CalendarEvent> calendar_events;


    private int month;
    private int year;

    private View rowView;
    private View emptyView;
    private TextView dateView;
    private TextView txtView;

    public CalendarArrayAdapter(Context context, ArrayList<CalendarEvent> values) {
        super(context, -1, values);
        this.context = context;
        this.calendar_events = values;
        Collections.sort(values, ALPHABETICAL_ORDER1);
    }

    public void setEvents(ArrayList<CalendarEvent> events)
    {
        calendar_events = events;
    }


    public void setCurrentDate(int month, int year)
    {
        this.month = month;
        this.year = year;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent)
    {
        setupView(parent);
        if(!isCorrectMonthForEvent(index)){
            return emptyView; //SHOW ONLY EVENTS FROM CURRENT MONTH
        }
        setUpRow(index);
        return rowView;
    }

    private boolean isCorrectMonthForEvent(int index) {
        return month == calendar_events.get(index).getMonth() && year == calendar_events.get(index).getYear();
    }

    private void setupView(ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        emptyView = inflater.inflate(R.layout.empty, parent, false);
        rowView = inflater.inflate(R.layout.calendarrowlayout, parent, false);
        dateView = rowView.findViewById(R.id.date);
        txtView = rowView.findViewById(R.id.title);
    }

    private String createHeader(int position)
    {
        String dateString = calendar_events.get(position).getDate();
        if(calendar_events.get(position).isAlarmSet()) dateString += " - "+ calendar_events.get(position).getAlarm();
        return dateString;
    }


    private void setUpRow(int index)
    {
        dateView.setText(createHeader(index));
        txtView.setText(calendar_events.get(index).getTitle());
    }

    Comparator<CalendarEvent> ALPHABETICAL_ORDER1 = new Comparator<CalendarEvent>() {
        public int compare(CalendarEvent object1, CalendarEvent object2) {
            return String.CASE_INSENSITIVE_ORDER.compare(object1.getDateForSort(), object2.getDateForSort());
        }
    };
}