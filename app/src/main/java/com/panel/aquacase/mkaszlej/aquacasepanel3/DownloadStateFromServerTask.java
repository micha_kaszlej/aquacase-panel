package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DownloadStateFromServerTask extends AsyncTask<String, String, String>
{
    private IAquariumStateDependent activity;

    private String url_get_state;

    private final String TAG = "AQUACASE";
    private final String PREFIX = "[DownloadStateFromServer] ";
    private final String TAG_SUCCESS = "success";
    private final String METHOD = "POST";
    private final String USER_ID = "userId";
    private final String USER_CODE = "userCode";
    private final String STATUS = "status";
    private final String MESSAGE_ID = "messageId";

    DownloadStateFromServerTask(String url, IAquariumStateDependent activity) {
        super();
        this.activity = activity;
        url_get_state = url;
        if (url.length() == 0 || activity == null) throw new AssertionError();
    }

    private JSONObject makeHttpRequest()
    {
        List<NameValuePair> newStatus = new ArrayList<>();
        newStatus.add(new BasicNameValuePair(USER_ID, ""+StateSingleton.getInstance().getState().userId));
        newStatus.add(new BasicNameValuePair(USER_CODE, ""+StateSingleton.getInstance().getState().userCode));
        newStatus.add(new BasicNameValuePair(MESSAGE_ID, ""+StateSingleton.getInstance().getState().messageId));
        return new JSONParser().makeHttpRequest(url_get_state, METHOD, newStatus);
    }

    private boolean isRequestOk(JSONObject json)
    {
        if (json != null) {
            if(JSONParser.server_response_code != 200) {
                Log.d(TAG, PREFIX + "HTTP error: " + JSONParser.server_response_code);
                return false;
            }
            Log.d(TAG, PREFIX + "server responded: " + json.toString());
            return true;
        }
        Log.d(TAG, PREFIX + "FAIL! message was null");
        return false;
    }

    private void processServerResponse(JSONObject json) throws JSONException
    {
        if (json.getInt(TAG_SUCCESS) == 1)
        {
            processSuccessMessage(json);
            Log.d(TAG, PREFIX + "Received state: " + json.toString());
        }
        else
            processFailureMessage();
    }

    private void processSuccessMessage(JSONObject json) throws JSONException {
        JSONObject status = json.getJSONArray(STATUS).getJSONObject(0);
        updateAquariumState(status, status.getInt(MESSAGE_ID));
    }

    private void updateAquariumState(JSONObject status, int message_id)
    {
        StateSingleton.getInstance().getState().update(status);
        StateSingleton.getInstance().setServerLastMessageId(message_id);
        NetworkSingleton.getInstance().setIsConnected(true);
    }

    private void processFailureMessage()
    {
        NetworkSingleton.getInstance().setIsConnected(false);
    }

    protected String doInBackground(String... params)
    {
        try {
            JSONObject json = makeHttpRequest();
            if (isRequestOk(json))
                processServerResponse(json);
        } catch (Exception e) {
            processFailureMessage();
            e.printStackTrace();
        }
        return null;
    }

    private void tryDownloadWorkcycles()
    {
        if (StateSingleton.getInstance().getWorkcycleId() > StateSingleton.getInstance().getServerLastWorkcycleId())
        {
            StateSingleton.getInstance().downloadWorkCyclesFromServer();
        }
    }

    private void tryDownloadCalendarEvents()
    {
        if (StateSingleton.getInstance().getCalendarEventId() > StateSingleton.getInstance().getServerLastCalendarEventId())
        {
            StateSingleton.getInstance().downloadCalendarEventsFromServer();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        tryDownloadWorkcycles();
        tryDownloadCalendarEvents();
        updateUI();
    }

    private void updateUI()
    {
        StateSingleton state = StateSingleton.getInstance();
        if(state.shouldShowNewScreen())
        {
            startActivityBasedOnStatus();
        }
        else if(state.shouldRefreshCurrentScreen())
        {
            activity.recreateFromState();
        }
    }

    private void startActivityBasedOnStatus() {
        StateSingleton s = StateSingleton.getInstance();
        s.setIsAutoSwitchingScreen();
        int screen_id = s.getServerCurrentScreenId();
        activity.startActivityBasedOnState(s.getActivityClassFromScreenId(screen_id), s.getActivityStartBundle(screen_id));
    }

}