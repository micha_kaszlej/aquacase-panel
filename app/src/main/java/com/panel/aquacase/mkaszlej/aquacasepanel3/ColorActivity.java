package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class ColorActivity extends AquaBaseActivity {

    boolean isInterior = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.COLOR.value;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        if (getIntent().getIntExtra("mode", 0) == 1)
        {
            isInterior = false;
        }

        intializeView();
    }

    @Override
    public void recreateFromState() {}

    private void intializeView()
    {
        //DECLARED IN VALUES.COLORS
        int[] rainbow = getResources().getIntArray(R.array.rainbow);

        for( int i = 0 ; i < 20; i++ )
        {
            int buttonId = this.getResources().getIdentifier("colorButton"+(i+1), "id", this.getPackageName());
            ImageButton button = findViewById(buttonId);
            if(button != null)
            {
                final int c = rainbow[i];
                button.setColorFilter(c, PorterDuff.Mode.MULTIPLY);
                Util.setStateDrawable(getApplicationContext(),button, R.drawable.color0, R.drawable.color1);

                //SAVE COLOR INFORMATION IN GLOBAL VARIABLE ON CLICK
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setSelected(true);
                        if (isInterior) StateSingleton.getInstance().setRgbColor(getApplicationContext(), c);
                        else  StateSingleton.getInstance().setOutRgbColor(c);
                        finish();
                    }
                });
            }
        }
    }

    public void backAction(View v)
    {
        startActivity(new Intent(this, RgbActivity.class));
        finish();
    }
}
