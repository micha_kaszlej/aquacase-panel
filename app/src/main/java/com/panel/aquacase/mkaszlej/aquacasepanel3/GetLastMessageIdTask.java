package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetLastMessageIdTask extends AsyncTask<String, String, String>
{
    private String url_get_state;

    private final String TAG = "AQUACASE";
    private final String PREFIX = "[GetLastMessageId] ";
    private final String TAG_SUCCESS = "success";
    private final String METHOD = "POST";
    private final String USER_ID = "userId";
    private final String USER_CODE = "userCode";
    private final String MESSAGE_ID = "messageId";

    GetLastMessageIdTask(String url) {
        url_get_state = url;
        if (url.length() == 0) throw new AssertionError();
    }

    private JSONObject makeHttpRequest()
    {
        List<NameValuePair> newStatus = new ArrayList<>();
        newStatus.add(new BasicNameValuePair(USER_ID, Integer.toString(StateSingleton.getInstance().getState().userId)));
        newStatus.add(new BasicNameValuePair(USER_CODE, Integer.toString(StateSingleton.getInstance().getState().userCode)));
        return new JSONParser().makeHttpRequest(url_get_state, METHOD, newStatus);
    }

    private boolean isRequestOk(JSONObject json)
    {
        if (json != null) {
            if(JSONParser.server_response_code != 200) {
                Log.d(TAG, PREFIX + "HTTP error: " + JSONParser.server_response_code);
                return false;
            }

            Log.d(TAG, PREFIX + "server responded: " + json.toString());
            return true;
        }
        Log.d(TAG, PREFIX + "FAIL! message was null");
        return false;
    }

    private void processServerResponse(JSONObject json) throws JSONException
    {
        if (json.getInt(TAG_SUCCESS) == 1)
        {
            processSuccessMessage(json);
        }
        else
            processFailureMessage();
    }

    private void processSuccessMessage(JSONObject json) throws JSONException {
        int message_id = json.getInt(MESSAGE_ID);
        if (message_id != StateSingleton.getInstance().getServerLastMessageId())
            Log.d(TAG, PREFIX + "Received last message id: " + message_id);

        updateAquariumState(message_id);
    }

    private void updateAquariumState(int message_id)
    {
        StateSingleton.getInstance().setServerLastMessageId(message_id);
        NetworkSingleton.getInstance().setIsConnected(true);
    }

    private void processFailureMessage()
    {
        NetworkSingleton.getInstance().setIsConnected(false);
    }

    protected String doInBackground(String... params)
    {
        try {
            JSONObject json = makeHttpRequest();
            if (isRequestOk(json))
                processServerResponse(json);
        } catch (Exception e) {
            processFailureMessage();
            e.printStackTrace();
        }
        return null;
    }
}