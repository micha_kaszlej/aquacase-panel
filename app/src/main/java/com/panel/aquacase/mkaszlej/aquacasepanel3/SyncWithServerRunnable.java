package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.app.Activity;
import android.os.Handler;

public class SyncWithServerRunnable implements Runnable
{
    IAquariumStateDependent activity_to_update = null;
    Handler syncWithServer = null;

    SyncWithServerRunnable(IAquariumStateDependent activity, Handler handler)
    {
        activity_to_update = activity;
        syncWithServer = handler;
    }

    @Override
    public void run() {
        if (shouldGetNewStateFromServer()){
            NetworkSingleton.getInstance().downloadStateFromServer(activity_to_update);
        }
        else
        {
            NetworkSingleton.getInstance().downloadLastServerMessageId();
        }
        if( activity_to_update.canSyncWithServer() )
        {
            syncWithServer.postDelayed(this, 500);
        }
    }

    private boolean shouldGetNewStateFromServer()
    {
        int last_server_message = StateSingleton.getInstance().getServerLastMessageId();
        int last_status_message = StateSingleton.getInstance().getMessageId();
        return last_server_message > last_status_message;
    }
}
