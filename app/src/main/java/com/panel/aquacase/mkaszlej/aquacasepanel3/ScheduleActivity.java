package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScheduleActivity extends AquaBaseActivity {

    int activity_to_schedule;
    WorkCycle w1 = null;
    WorkCycle w2 = null;
    WorkCycle w3 = null;
    LinearLayout work3;
    LinearLayout work2;
    LinearLayout work1;

    TextView firstCycleTimes;
    TextView secondCycleTimes;
    TextView thirdCycleTimes;

    TextView captionText;
    TextView additionalInfo;
    TextView firstCycleLabel;

    ImageView screenIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.SCHEDULE.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        activity_to_schedule = 0;
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            activity_to_schedule = extras.getInt("TYPE", 0);
        }

        intializeView();
        recreateFromState();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        StateSingleton.getInstance().setCurrentScreenType(activity_to_schedule);

        recreateFromState();
    }

    private void intializeView()
    {
        work3 = findViewById( R.id.work3 );
        work2 = findViewById( R.id.work2 );
        work1 = findViewById( R.id.work1 );

        firstCycleTimes = findViewById( R.id.firstCycleTimes );
        secondCycleTimes = findViewById( R.id.secondCycleTimes );
        thirdCycleTimes = findViewById( R.id.thirdCycleTimes );

        captionText = findViewById( R.id.captionText );
        additionalInfo = findViewById( R.id.additionalInfo );
        firstCycleLabel = findViewById( R.id.firstCycleLabel );

        screenIcon = findViewById( R.id.screenIcon );

        work1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                v.setSelected(true);
                Intent i = new Intent( ScheduleActivity.this , TimeSelectActivity.class );

                i.putExtra("TYPE", activity_to_schedule);
                i.putExtra("STARTH", w1.getStartHour() );
                i.putExtra("STARTM", w1.getStartMinute() );
                i.putExtra("ENDH", w1.getEndHour() );
                i.putExtra("ENDM", w1.getEndMinute() );
                i.putExtra("CYCLEID", w1.getId() );
                i.putExtra("ISSET", w1.isSet() );
                i.putExtra("WORKCYCLE", R.string.firstWorkCycle);

                startActivity(i);
                return true;
            }
        });

        work2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                v.setSelected(true);
                Intent i = new Intent( ScheduleActivity.this , TimeSelectActivity.class );

                i.putExtra("TYPE", activity_to_schedule);
                i.putExtra("STARTH", w2.getStartHour() );
                i.putExtra("STARTM", w2.getStartMinute() );
                i.putExtra("ENDH", w2.getEndHour() );
                i.putExtra("ENDM", w2.getEndMinute());
                i.putExtra("CYCLEID", w2.getId() );
                i.putExtra("ISSET", w2.isSet() );
                i.putExtra("WORKCYCLE",R.string.secondWorkCycle);

                startActivity(i);
                return true;
            }
        });

        work3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                v.setSelected(true);
                Intent i = new Intent(ScheduleActivity.this, TimeSelectActivity.class);

                i.putExtra("TYPE", activity_to_schedule);
                i.putExtra("STARTH", w3.getStartHour());
                i.putExtra("STARTM", w3.getStartMinute());
                i.putExtra("ENDH", w3.getEndHour());
                i.putExtra("ENDM", w3.getEndMinute());
                i.putExtra("CYCLEID", w3.getId());
                i.putExtra("ISSET", w3.isSet());
                i.putExtra("WORKCYCLE", R.string.thirdWorkCycle);

                startActivity(i);
                return true;
            }
        });
    }

    @Override
    public void recreateFromState()
    {
        if( activity_to_schedule == AquariumState.ActivityType.LIGHT.value){
            captionText.setText(R.string.scheduleLight);
            screenIcon.setImageResource(R.drawable.lightscreenicon);
            additionalInfo.setVisibility(View.VISIBLE);
            additionalInfo.setText(R.string.lightScheduleInfo);
        }
        else if( activity_to_schedule == AquariumState.ActivityType.RGB.value){
            captionText.setText(R.string.scheduleRGB);
            screenIcon.setImageResource(R.drawable.rgbscreen);
            additionalInfo.setVisibility(View.VISIBLE);
            additionalInfo.setText(R.string.rgbScheduleInfo);
        }
        else if( activity_to_schedule == AquariumState.ActivityType.FEED.value){
            captionText.setText(R.string.scheduleFood);
            screenIcon.setImageResource( R.drawable.foodscreen );
            work3.setVisibility( View.GONE );
        }
        else if( activity_to_schedule == AquariumState.ActivityType.O2.value){
            captionText.setText(R.string.scheduleO2);
            screenIcon.setImageResource(R.drawable.o2screen);
        }
        else if( activity_to_schedule == AquariumState.ActivityType.CO2.value){
            captionText.setText(R.string.scheduleCO2);
            screenIcon.setImageResource(R.drawable.co2screen);
            work3.setVisibility(View.GONE);
            work2.setVisibility(View.GONE);
            additionalInfo.setVisibility(View.VISIBLE);
            firstCycleLabel.setText(R.string.workTime);
            additionalInfo.setText(R.string.co2ScheduleInfo);
        }
        else if( activity_to_schedule == AquariumState.ActivityType.LPUMP.value){
            captionText.setText(R.string.scheduleLPump);
            screenIcon.setImageResource( R.drawable.pumpscreenicon );
        }
        else if( activity_to_schedule == AquariumState.ActivityType.RPUMP.value){
            captionText.setText(R.string.scheduleRPump);
            screenIcon.setImageResource( R.drawable.pumpscreenicon );
        }
        else if( activity_to_schedule == AquariumState.ActivityType.VOLCANO.value){
            captionText.setText(R.string.scheduleVolcano);
            screenIcon.setImageResource( R.drawable.volcanoscreenicon );
            additionalInfo.setVisibility(View.VISIBLE);
            additionalInfo.setText(R.string.volcanoScheduleInfo);
        }

        for( WorkCycle c : StateSingleton.getInstance().getWorkCycle(activity_to_schedule) ){
            switch (c.getId()){
                case 0:
                    w1 = c;
                    work1.setSelected(c.isSet() > 0);
                    firstCycleTimes.setText( c.toString() );
                    break;
                case 1:
                    w2 = c;
                    work2.setSelected( c.isSet() > 0 );
                    secondCycleTimes.setText(c.toString());
                    break;
                case 2:
                    w3 = c;
                    work3.setSelected( c.isSet() > 0 );
                    thirdCycleTimes.setText(c.toString());
                    break;
                default:
                    break;
            }
        }

        if (w1 == null)
        {
            work1.setVisibility(View.GONE);
        }
        if (w2 == null)
        {
            work2.setVisibility(View.GONE);
        }
        if (w3 == null)
        {
            work3.setVisibility(View.GONE);
        }
    }

    public void updateTimeButton( View v )
    {
        if(v.isSelected()){
            v.setSelected(false);
            StateSingleton.getInstance().deactivateCycle(w1.getType(), w1.getId());
        }
        else{
            v.setSelected(true);
            StateSingleton.getInstance().activateCycle(w1.getType(), w1.getId());
        }
    }

    public void updateTimeButton2( View v )
    {
        if(v.isSelected()){
            v.setSelected(false);
            StateSingleton.getInstance().deactivateCycle(w2.getType(), w2.getId());
        }
        else{
            v.setSelected(true);
            StateSingleton.getInstance().activateCycle(w2.getType(), w2.getId());
        }
    }

    public void updateTimeButton3( View v )
    {
        if(v.isSelected()){
            v.setSelected(false);
            StateSingleton.getInstance().deactivateCycle(w3.getType(), w3.getId());
        }
        else{
            v.setSelected(true);
            StateSingleton.getInstance().activateCycle(w3.getType(), w3.getId());
        }
    }

    public void backAction(View v)
    {
        startActivity(new Intent(this,MainFragmentActivity.class));
        finish();
    }

}
