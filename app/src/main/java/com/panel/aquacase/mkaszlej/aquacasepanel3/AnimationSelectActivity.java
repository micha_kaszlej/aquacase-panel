package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AnimationSelectActivity extends AquaBaseActivity {

    boolean isInterior = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        activity_type = AquariumState.ActivityType.ANIMATIONSELECT.value;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation_select);

        if (getIntent().getIntExtra("mode", 0) == 1)
        {
            isInterior = false;
        }
    }

    @Override
    public void recreateFromState() {}

    public void setAnimationType(int type)
    {
        if (isInterior)
        {
            StateSingleton.getInstance().setRgbAnimationType(type);
        }
        else
        {
            StateSingleton.getInstance().setOutRgbAnimtionType(type);
        }
    }

    public void backAction(View v)
    {
        startActivity(new Intent(this, RgbActivity.class));
        finish();
    }

    public void animation1Action(View v){
        setAnimationType(0);
        finish();
    }

    public void animation2Action(View v){
        setAnimationType(1);
        finish();
    }

    public void animation3Action(View v){
        setAnimationType(2);
        finish();
    }
    public void animation4Action(View v){
        setAnimationType(3);
        finish();
    }
    public void animation5Action(View v){
        setAnimationType(4);
        finish();
    }
}