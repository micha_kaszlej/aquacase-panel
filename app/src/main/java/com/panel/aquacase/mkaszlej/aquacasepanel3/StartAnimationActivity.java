package com.panel.aquacase.mkaszlej.aquacasepanel3;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class StartAnimationActivity extends AquaBaseActivity {

    ImageView animWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        activity_type = AquariumState.ActivityType.ANIMATIONSHOW.value;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation_show);
        initializeView();
    }

    @Override
    public void onResume() {
        super.onResume();
        recreateFromState();
        StateSingleton.getInstance().readSharedPreferences(this);
//        StateSingleton.getInstance().downloadStateFromServer();
        new Handler().postDelayed(new Runnable() {public void run() { StartApplication(); }}, 3000);
    }

    // TODO: Attach bg animation here when its ready (probably never:P GL GJ !)
    private void initializeView() {
        animWrapper = findViewById(R.id.animationWrapper);
    }

    @Override
    public void recreateFromState() {
        Util.setNormalDrawable(getApplicationContext(),animWrapper,R.drawable.start0, 512, 511);
    }

    private void StartApplication() {
        if(NetworkSingleton.getInstance().isConnected()) {
            if(StateSingleton.getInstance().IsPinCheckRequired())
                startActivity(new Intent(this, PinCodeActivity.class));
            else
                startActivity(new Intent(this, LockActivity.class));
        }
        else
            startActivity(new Intent(this, ConnectActivity.class));
        finish();
    }

}
