package com.panel.aquacase.mkaszlej.aquacasepanel3;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by mkaszlej on 2016-02-22.
 */
public class WorkCycle {

    private int type;
    private int id;
    private int isSet, isEdited;
    private int hStart , mStart, hEnd, mEnd;

    WorkCycle( int type , int id, int hStart, int mStart, int hEnd, int mEnd, int isSet ){
        this.type = type;
        this.id = id;
        this.hStart = hStart;
        this.hEnd = hEnd;
        this.mStart = mStart;
        this.mEnd = mEnd;
        this.isSet = isSet;
        isEdited=0;
    }

    public void setActive( int a ){
        this.isSet = a;
    }

    public void update( int hStart, int mStart, int hEnd, int mEnd, int isSet ){
        this.hStart = hStart;
        this.hEnd = hEnd;
        this.mStart = mStart;
        this.mEnd = mEnd;
        this.isSet = isSet;
    }

    public String toString(){
        String minS = ""+mStart;
        if(mStart < 10) minS = "0"+mStart;
        String minE = ""+mEnd;
        if(mEnd < 10) minE = "0"+mEnd;

        if(type == AquariumState.ActivityType.FEED.value ) return hStart+":"+minS;
        if(type == AquariumState.ActivityType.CO2.value ) return minS + " min";
        return hStart+":"+minS+" - "+hEnd+":"+minE;
    }

    public String toJSON(){
        return "{\"type\":"+type+",\"id\":"+id+",\"isSet\":"+isSet+",\"hStart\":"+hStart+",\"mStart\":"+mStart+",\"hEnd\":"+hEnd+",\"mEnd\":"+mEnd+"}";
    }

    public int getType(){ return type; }
    public int getId(){ return id; }
    public int isSet(){ return isSet; }
    public int getStartHour(){return hStart; }
    public int getStartMinute(){return mStart; }
    public int getEndHour(){return hEnd; }
    public int getEndMinute(){return mEnd; }


    public ArrayList<NameValuePair> getData(){

        ArrayList<NameValuePair> l = new ArrayList<NameValuePair>();
        l.add(new BasicNameValuePair("userId", Integer.toString(StateSingleton.getInstance().getUserId())));
        l.add(new BasicNameValuePair("messageId", Integer.toString(StateSingleton.getInstance().getAndIncrementMessageId())));
        l.add(new BasicNameValuePair("userCode", Integer.toString(StateSingleton.getInstance().getUserCode())));
        l.add(new BasicNameValuePair("isAquarium", Integer.toString(0)));
        l.add(new BasicNameValuePair("type", Integer.toString(type)  ));
        l.add(new BasicNameValuePair("idInType", Integer.toString(id)));
        l.add(new BasicNameValuePair("isSet", Integer.toString(isSet)));
        l.add(new BasicNameValuePair("isEdited", Integer.toString(isEdited)));
        l.add(new BasicNameValuePair("hStart", Integer.toString(hStart)));
        l.add(new BasicNameValuePair("mStart", Integer.toString(mStart)));
        l.add(new BasicNameValuePair("hEnd", Integer.toString(hEnd)));
        l.add(new BasicNameValuePair("mEnd", Integer.toString(mEnd)));

        return l;
    }
}
