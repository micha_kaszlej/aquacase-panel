<?php

	///////////////////////////////////////////////////////////////////////////////////////

	//CHECK USER AUTHORISATION CODE
	//ASSUMES WE ARE CONNECTED TO DB
	function checkUserAuthCode( $userId, $userCode, $debug=0 )
	{
		if($debug)
			echo "USER_ID: ".$userId," USER_CODE: ".$userCode."\n";
			
		$code_check = mysql_query("SELECT user_code FROM aquacase_user WHERE user_id = '$userId'");
		if(!$code_check)
		{
			//NO SUCH USER
			$response["success"] = -3;
			$response["message"] = "QUERY CANNOT BE EXECUTED!";
	 
			// echoing JSON response
			echo json_encode($response);
			return false;
		}
		
		$rows_found = mysql_num_rows($code_check);
		if( $rows_found == 0 )
		{
			//NO SUCH USER
			$response["success"] = -1;
			$response["message"] = "NO SUCH USER!";
	 
			// echoing JSON response
			echo json_encode($response);
			return false;
		}
		
		$code_arr = mysql_fetch_row($code_check);
		if(!$code_arr || $code_arr[0] <> $userCode)
		{
			//WRONG CODE
			$response["success"] = -2;
			$response["message"] = "WRONG CODE!";

			// echoing JSON response
			echo json_encode($response);
			return false;
		}

		if($debug)
			echo "USER AUTH CORRECT\n";		
		
		//USER AND CODE CORRECT
		return true;
		
	}

	///////////////////////////////////////////////////////////////////////////////////////
	//CHECK IF RECEIVED MESSAGE_ID IS NEWER THAN MESSAGE_ID IN DB
	function checkMessageId($userId, $messageId, $debug=0)
	{
		if($debug)
			echo "MESSAGE ID: ".$messageId."\n";
		
		$message_id_row = mysql_query("SELECT messageId, FROM_UNIXTIME(aquacase_status.currentTime/1000) AS 'czas' FROM aquacase_status WHERE aquacase_status.userId = '$userId' ORDER BY messageId DESC LIMIT 1"); // ORDER BY currentTime DESC -> ORDER BY messageId DESC
		if(!$message_id_row)
		{
			//NO SUCH USER
			$response["success"] = -3;
			$response["message"] = "MESSAGE_ID QUERY CANNOT BE EXECUTED!";
	 
			// echoing JSON response
			echo json_encode($response);
			return false;
		}
		
		$rows_found = mysql_num_rows($message_id_row);
		if( $rows_found == 0 )
			//FIRST STATUS FOR THIS USER - ALLOW
			return true;
		
		$messageIDFormDBRow = mysql_fetch_row($message_id_row);
		if( $messageIDFormDBRow[0] >= $messageId )
		{
			if($debug)
				echo "DB MESSAGE ID: ".$messageIDFormDBRow[0]."\n";
			
			//WRONG CODE
			$response["success"] = -4;
			$response["message"] = "WRONG MESSAGE_ID!";

			// echoing JSON response
			echo json_encode($response);
			return false;
			
		}

		return true;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	//CHECK IF HAS DEFINIED WORK CYCLES
	function hasDefinedCycles($userId)
	{
		$result = mysql_query("SELECT * FROM aquacase_workcycle WHERE aquarium_id = '$userId'");

		// check for corrupted work cycles
		if (mysql_num_rows($result) < 21) {
			fillDefaultWorkCycles($userId);
		}
		
		return true;
			
	}
	
	///////////////////////////////////////////////////////////////////////////////////////

	/**
	 * A class file to connect to database
	 */
	class DB_CONNECT {
	 
		// constructor
		function __construct() {
			// connecting to database
			$this->connect();
		}
	 
		// destructor
		function __destruct() {
			// closing db connection
			$this->close();
		}
	 
		/**
		 * Function to connect with database
		 */
		function connect() {

			define('DB_DATABASE', 'aquacaseaqua');
			define('DB_USER', 'aquacaseaqua');
			define('DB_PASSWORD', 'Globcapital3040');
			define('DB_SERVER', 'aquacaseaqua.mysql.db');


			// Connecting to mysql database
			$con = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());
	 
			// Selecing database
			$db = mysql_select_db(DB_DATABASE) or die(mysql_error()) or die(mysql_error());
	 
			// returing connection cursor
			return $con;
		}
	 
		/**
		 * Function to close db connection
		 */
		function close() {
			// closing db connection
			mysql_close();
		}
	 
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////
		
	function fillDefaultWorkCycles($aquariumId){

		//REMOVE ALL NOT CORRECT CYCLES
		mysql_query("DELETE FROM aquacase_workcycle WHERE aquariumId = '$aquariumId'");
	
		//FEED1  workCycles.add( new WorkCycle( 16, 0 , 8 , 0 , -1 , -1 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES( '$aquariumId', 16, 0, 8, 0, -1, -1, 1, 0 ) ;");

		//FEED2 workCycles.add( new WorkCycle( 16 , 1 , 17 , 0 , -1 , -1 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES( '$aquariumId' , 16, 1, 17, 0, -1, -1, 1, 0 ) ;");

		//O2 workCycles.add( new WorkCycle( 17 , 0 , 8 , 0 , 10 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES( '$aquariumId', 17, 0, 8, 0, 10, 0, 1, 0 ) ;");

		//O2workCycles.add( new WorkCycle( 17 , 1 , 14 , 0 , 18 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES( '$aquariumId', 17, 1, 14, 0, 18, 0, 1, 0 ) ;");

		//O2workCycles.add( new WorkCycle( 17 , 2 , 20 , 0 , 22 , 0 , 0 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES( '$aquariumId', 17, 2, 20, 0, 22, 0, 0, 0 ) ;");

		//VOLCANO workCycles.add( new WorkCycle( 21 , 0 , 8 , 0 , 10 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 21, 0, 8, 0, 10, 0, 1, 0 ) ;");

		//VOLCANO workCycles.add( new WorkCycle( 21 , 1 , 14 , 0 , 18 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 21, 1, 14, 0, 18, 0, 1, 0 ) ;");

		//VOLCANO workCycles.add( new WorkCycle( 21 , 2 , 20 , 0 , 22 , 0 , 0 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 21, 2, 20, 0, 22, 0, 0, 0 ) ;");

		//CO2 workCycles.add( new WorkCycle( 18 , 0 , -1 , 30 , -1 , -1 , -1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 18, 0, -1, 30, -1, -1, 1, 0 ) ;");

		//LPUMP workCycles.add( new WorkCycle( 19 , 0 , 0 , 0 , 7 , 30 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 19, 0, 0, 0, 7, 30, 1, 0 ) ;");

		//LPUMP workCycles.add( new WorkCycle( 19 , 1 , 8 , 0 , 21 , 30 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 19, 1, 8, 0, 21, 30, 1, 0 ) ;");

		//LPUMP workCycles.add( new WorkCycle( 19 , 2 , 22 , 0 , 23 , 59 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 19, 2, 22, 0, 23, 59, 1, 0 ) ;");

		//RPUMP workCycles.add( new WorkCycle( 20 , 0 , 0 , 0 , 2 , 30 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 20, 0, 0, 0, 2, 30, 1, 0 ) ;");

		//RPUMP workCycles.add( new WorkCycle( 20 , 1 , 3 , 0 , 13 , 30 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 20, 1, 3, 0, 13, 30, 1, 0 ) ;");

		//RPUMP workCycles.add( new WorkCycle( 20 , 2 , 14 , 0 , 23 , 59 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 20, 2, 14, 0, 23, 59, 1, 0 ) ;");

		//LIGHT workCycles.add( new WorkCycle( 8 , 0 , 9 , 0 , 11 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 8, 0, 9, 0, 11, 0, 1, 0 ) ;");
		
		//LIGHT workCycles.add( new WorkCycle( 8 , 1 , 14 , 0 , 17 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 8, 1, 14, 0, 17, 0, 1, 0 ) ;");
		
		//LIGHT workCycles.add( new WorkCycle( 8 , 2 , 19 , 0 , 21 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 8, 2, 19, 0, 21, 0, 1, 0 ) ;");

		//RGB workCycles.add( new WorkCycle( 10 , 0 , 9 , 0 , 11 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 10, 0, 9, 0, 11, 0, 1, 0 ) ;");

		//RGB workCycles.add( new WorkCycle( 10 , 1 , 14 , 0 , 17 , 0 , 1 ) );
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 10, 1, 14, 0, 17, 0, 1, 0 ) ;");

		//RGB workCycles.add(new WorkCycle( 10 , 2, 19, 0, 21, 0, 1));
		mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', 10, 2, 19, 0, 21, 0, 1, 0 ) ;");

	}
	
?>