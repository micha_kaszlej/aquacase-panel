<?php
 
require_once("functions.php");

// check for required fields
if (isset($_POST['userId']) && isset($_POST['userCode'])) 
{
  
	$userId = $_POST['userId'];
	$userCode = $_POST['userCode'];
	
    // connecting to db
    $db = new DB_CONNECT();
	 
	if( checkUserAuthCode($userId, $userCode) )
	{
		$result = mysql_query("SELECT * FROM aquacase_status WHERE userId = $userId AND userCode = $userCode ORDER BY messageId DESC LIMIT 1");
	 
		if (!empty($result)) 
		{

			// check for empty result
			if (mysql_num_rows($result) > 0) {
	 
				$result = mysql_fetch_array($result);
	 
				$status = array();
				$status["userId"] = $result["userId"];
				$status["messageId"] = $result["messageId"];
				$status["userCode"] = $result["userCode"];
				$status["isAquarium"] = $result["isAquarium"];
				$status["currentTime"] = $result["currentTime"];
				$status["mode"] = $result["mode"];
				$status["currentScreenId"] = $result["currentScreenId"];
				$status["pinCode"] = $result["pinCode"];
				$status["leftPumpActive"] = $result["leftPumpActive"];
				$status["rightPumpActive"] = $result["rightPumpActive"];
				$status["lightActive"] = $result["lightActive"];
				$status["rgbActive"] = $result["rgbActive"];
				$status["outRgbActive"] = $result["outRgbActive"];
				$status["o2Active"] = $result["o2Active"];
				$status["volcanoActive"] = $result["volcanoActive"];
				$status["co2Active"] = $result["co2Active"];
				$status["foodActive"] = $result["foodActive"];
				$status["lightPower"] = $result["lightPower"];
				$status["currentTemperature"] = $result["currentTemperature"];
				$status["desiredTemperature"] = $result["desiredTemperature"];
				$status["rgbAnimationType"] = $result["rgbAnimationType"];
				$status["rgbPower"] = $result["rgbPower"];
				$status["outRgbAnimationType"] = $result["outRgbAnimationType"];
				$status["outRgbPower"] = $result["outRgbPower"];
				$status["rgbColor"] = $result["rgbColor"];
				$status["outRgbColor"] = $result["outRgbColor"];
				$status["languageCode"] = $result["languageCode"];
				$status["bgResourceId"] = $result["bgResourceId"];
				$status["dispTimeOff"] = $result["dispTimeOff"];
				$status["dispTimeLock"] = $result["dispTimeLock"];
	 
				// success
				$response["success"] = 1;
	 
				// user node
				$response["status"] = array();
	 
				array_push($response["status"], $status);
	 
				// echoing JSON response
				echo json_encode($response);
				
			} else {
				// no status found
				$response["success"] = 0;
				$response["message"] = "No status found";
	 
				echo json_encode($response);
			}
		} 
		else 
		{
			// no status found
			$response["success"] = 0;
			$response["message"] = "No status found";
	 
			// echo no users JSON
			echo json_encode($response);
		}
	}
}
else 
{
        $response["success"] = 0;
        $response["message"] = "Requred field(s) missing";
 
        // echo no users JSON
        echo json_encode($response);
}

?>
