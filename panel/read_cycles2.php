<?php
 
require_once("functions.php");

// connecting to db
$db = new DB_CONNECT();

getWorkCycles();

function getWorkCycles(){
	 
	// check for required fields
	if (isset( $_POST['aquariumId']) && isset( $_POST['userCode']) ) {
	  
		$userId = $_POST['aquariumId'];
		$userCode = $_POST['userCode'];
			
		if (checkUserAuthCode($userId, $userCode))
		{
			
			//TODO CHECK messageId
			$result = mysql_query("SELECT * FROM aquacase_workcycle WHERE aquarium_id = '$userId'");

			// check for empty result
			if (mysql_num_rows($result) > 20) {
				
				$response["workcycles"] = array();
				$response["aquariumId"] = $userId;
				$response["userCode"] = $userCode;
			 
				while ($row = mysql_fetch_array($result)) {
					// temp user array
					$workcycle = array();
					//$workcycle["a"] = $row["aquarium_id"];
					$workcycle["t"] = $row["type"];
					$workcycle["id"] = $row["id_in_type"];
					$workcycle["hs"] = $row["h_start"];
					$workcycle["ms"] = $row["m_start"];
					$workcycle["he"] = $row["h_end"];
					$workcycle["me"] = $row["m_end"];
					$workcycle["is"] = $row["is_set"];
					//$workcycle["i"] = $row["is_edited"];
			 
					array_push($response["workcycles"], $workcycle);
				}
				// success
				$response["success"] = 1;
			 
				// echoing JSON response
				echo str_replace( '"','', json_encode($response) ); // LESS DATA TO TRANSFER
			} 
			else 
			{
				//NO WORKCYCLES FOUND - FILL WITH DEFAULT VALUES
				fillDefaultWorkCycles($userId);
				//SEARCH AGAIN
				getWorkCycles();
			}
		}
	}
	else 
	{
			$response["success"] = 0;
			$response["message"] = "Required field(s) missing";
	 
			// echo no users JSON
			echo json_encode($response);
	}
	
	
}


?>
