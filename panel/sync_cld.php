<?php
	require_once("functions.php");
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
 // taskId - task ptr in aquarium
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_POST['userId']) && isset($_POST['aquariumId']) && isset($_POST['userCode']) && isset($_POST['taskId']) ) 
{
	
	$taskId = $_POST['taskId'];
	if( isset($_POST['isAquarium']) )
	{
		$aquaAck = 1;
		$mobileAck = 0;
	}
	$isAquarium = $_POST['isAquarium'];
	$aquariumId = $_POST['aquariumId'];
	$userId = $_POST['userId'];
	$userCode = $_POST['userCode'];
	$date = $_POST['date'];
	$time = $_POST['time'];
	$alarm = $_POST['alarm'];
	$txt = $_POST['txt'];
 
    // connecting to db
    $db = new DB_CONNECT();
		
	if( checkUserAuthCode($userId, $userCode) )//todo check taskid
	{
	
		$result = mysql_query("SELECT * FROM aquacase_cld WHERE aquariumId = $aquariumId AND taskId = $taskId ORDER BY timestamp DESC LIMIT 1");

		if (!empty($result)) 
		{
			if (mysql_num_rows($result) > 0)
			{
				//update task by taskId
				$result = mysql_fetch_array($result);
				if( $result["aquaAck"] == 1 )
				{
					$result = mysql_query("UPDATE aquacase_cld SET isAquarium='$isAquarium', aquariumId='$aquariumId', userId='$userId', 
											userCode='$userCode' , date='$date', time='$time', alarm='$alarm', txt='$txt'
											WHERE taskId = $taskId ORDER BY timestamp DESC ");	
						
					// successfully updated into database
					$response["success"] = 1;
					$response["message"] = "CalendarSync: TaskIsSync.";
					$response["userId"] = $aquariumId; 
					$response["taskId"] = $taskId;
					$response["tekst"] = $txt; 
			 
					// echoing JSON response
					echo json_encode($response);
				}
			}
			else
			{
				$result = mysql_query("INSERT INTO aquacase_cld( taskId, isAquarium, aquariumId, userId, userCode , aquaAck, mobileAck, date, time, alarm, txt )
										VALUES( '$taskId', '$isAquarium', '$aquariumId', '$userId', '$userCode', '$aquaAck', '$mobileAck', '$date', '$time', '$alarm', '$txt')");
											
				// check if row inserted or not
				if ($result) 
				{
					// successfully inserted into database
					$response["success"] = 1;
					$response["message"] = "CalendarSync: Row successfully created.";
					$response["userId"] = $aquariumId; 
					$response["taskId"] = $taskId; 
			 
					// echoing JSON response
					echo json_encode($response);
				} 
				else 
				{
					// failed to insert row
					$response["success"] = 0;
					$response["message"] = "CalendarSync: Oops! An error occurred.";
					$response["userId"] = $aquariumId; 
					$response["taskId"] = $taskId;
			 
					// echoing JSON response
					echo json_encode($response);
				}
			}
		}
		else
		{
			// successfully inserted into database
			$response["success"] = 1;
			$response["message"] = "CalendarSync: Error";
			$response["userId"] = $aquariumId; 
			$response["taskId"] = $taskId; 
		 
				// echoing JSON response
			echo json_encode($response);
		}

	}//FINISH
	
} 
else 
{
	
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "CalendarSync: Required field(s) missing";
	$response["userId"] = $aquariumId; 
	$response["taskId"] = $taskId;
 
    // echoing JSON response
    echo json_encode($response);
}


 

?>
