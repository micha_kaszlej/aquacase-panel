<?php
 
require_once("functions.php");

// check for required fields
if ( isset($_POST['userId']) && isset($_POST['userCode']) && isset($_POST['aquariumId']) ) 
{
	$userId = $_POST['userId'];
	$userCode = $_POST['userCode'];
	$aquariumId = $_POST['aquariumId'];
	
    // connecting to db
    $db = new DB_CONNECT();
	 
	if( checkUserAuthCode($userId, $userCode) )
	{
		if( isset($_POST['num']) )
		{
			$num = $_POST['num'];
			$result = mysql_query("SELECT * FROM aquacase_cld WHERE aquariumId = $aquariumId AND userCode = $userCode AND num = $num ORDER BY timestamp DESC LIMIT 1");
		}
		else
		{
			if( isset($_POST['taskId']) )
			{
				$taskId = $_POST['taskId'];
				$result = mysql_query("SELECT * FROM aquacase_cld WHERE aquariumId = $aquariumId AND userCode = $userCode AND taskId = $taskId ORDER BY timestamp DESC LIMIT 1");
			}
		}
	 
		if (!empty($result)) 
		{
			// check for empty result
			if (mysql_num_rows($result) > 0) 
			{
				$result = mysql_fetch_array($result);
	 
				$cldTask = array();
				$cldTask["num"] = $result["num"];
				$cldTask["taskId"] = $result["taskId"];
				$cldTask["deleteTask"] = $result["deleteTask"];
				$cldTask["isAquarium"] = $result["isAquarium"];
				$cldTask["aquariumId"] = $result["aquariumId"];
				$cldTask["userId"] = $result["userId"];
				$cldTask["userCode"] = $result["userCode"];
				$cldTask["aquaAck"] = $result["aquaAck"];
				$cldTask["mobileAck"] = $result["mobileAck"];
				$cldTask["date"] = $result["date"];
				$cldTask["time"] = $result["time"];
				$cldTask["alarm"] = $result["alarm"];
				$cldTask["txt"] = $result["txt"];

				// success
				$response["success"] = 1;
	 
				// user node
				$response["cldTask"] = array();
	 
				array_push($response["cldTask"], $cldTask);
	 
				// echoing JSON response
				echo str_replace( '"','', json_encode($response) );
				
			} else {
				// no status found
				$response["success"] = 0;
				$response["message"] = "No calendar task found";
	 
				echo json_encode($response);
			}
		} 
		else 
		{
			// no status found
			$response["success"] = 0;
			$response["message"] = "No calendar task found";
	 
			// echo no users JSON
			echo json_encode($response);
		}
	}
}
else 
{
        $response["success"] = 0;
        $response["message"] = "CldTask: Requred field(s) missing";
 
        // echo no users JSON
        echo json_encode($response);
}

?>
