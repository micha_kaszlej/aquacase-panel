<?php
	require_once("functions.php");
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_POST['userId']) && isset($_POST['messageId']) && isset($_POST['userCode'])) {
 
	$userId = $_POST['userId'];
	$messageId = $_POST['messageId'];
	$userCode = $_POST['userCode'];
	$isAquarium = $_POST['isAquarium'];
	$currentTime = $_POST['currentTime'];
	$mode = $_POST['mode'];
	$currentScreenId = $_POST['currentScreenId'];
	$currentScreenType = $_POST['currentScreenType'];
	$pinCode = $_POST['pinCode'];
	$leftPumpActive = $_POST['lpump'];
	$rightPumpActive = $_POST['rpump'];
	$lightActive = $_POST['light'];
	$rgbActive = $_POST['rgb'];
	$outRgbActive = $_POST['outRgb'];
	$o2Active = $_POST['o2'];
	$volcanoActive = $_POST['volcano'];
	$co2Active = $_POST['co2'];
	$foodActive = $_POST['food'];
	$lightPower = $_POST['lightPower'];
	$currentTemperature = $_POST['currentT'];
	$desiredTemperature = $_POST['desiredT'];
	$rgbAnimationType = $_POST['RGBAnim'];
	$rgbPower = $_POST['RGBPower'];
	$outRgbAnimationType = $_POST['outRGBAnim'];
	$outRgbPower = $_POST['outRGBPower'];
	$rgbColor = $_POST['RGBColor'];
	$outRgbColor = $_POST['outRGBColor'];
	$languageCode = $_POST['lang'];
	$bgResourceId = $_POST['bg'];
	$dispTimeOff = $_POST['dispOff'];
	$dispTimeLock = $_POST['dispLock'];
	$rgbColorNo = $_POST['rgbColNo'];
	$outRgbColorNo = $_POST['outRgbColNo'];
 
	$calendar[] = $_POST['calendar'];
	$workCycles[] = $_POST['workCycles'];
 
 
    // connecting to db
    $db = new DB_CONNECT();
		
	if( checkUserAuthCode($userId, $userCode) && checkMessageId($userId, $messageId) )
	{
		
		// mysql inserting a new row
		$result = mysql_query("INSERT INTO aquacase_status(userId, messageId, userCode, isAquarium, currentTime, mode, currentScreenId, currentScreenType, pinCode, 
		leftPumpActive, rightPumpActive, lightActive, rgbActive, outRgbActive, o2Active, volcanoActive, co2Active, 
		foodActive, lightPower, currentTemperature, desiredTemperature, rgbAnimationType, rgbPower, outRgbAnimationType, outRgbPower,
		rgbColor, outRgbColor, languageCode, bgResourceId, dispTimeOff, dispTimeLock, rgbColNo, outRgbColNo )
		VALUES('$userId', '$messageId', '$userCode', '$isAquarium', '$currentTime', '$mode', '$currentScreenId', '$currentScreenType', '$pinCode', 
		'$leftPumpActive', '$rightPumpActive', '$lightActive', '$rgbActive', '$outRgbActive', '$o2Active', '$volcanoActive', '$co2Active', 
		'$foodActive', '$lightPower', '$currentTemperature', '$desiredTemperature', '$rgbAnimationType', '$rgbPower', '$outRgbAnimationType', '$outRgbPower',
		'$rgbColor', '$outRgbColor', '$languageCode', '$bgResourceId', '$dispTimeOff', '$dispTimeLock', '$rgbColorNo', '$outRgbColorNo')");
	 
		// check if row inserted or not
		if ($result) {
			// successfully inserted into database
			$response["success"] = 1;
			$response["message"] = "Row successfully created.";
			$response["userId"] = $userId; 
			$response["messageId"] = $messageId; 
	 
			// echoing JSON response
			echo json_encode($response);
		} else {
			// failed to insert row
			$response["success"] = 0;
			$response["message"] = "Oops! An error occurred.";
			$response["userId"] = $userId; 
			$response["messageId"] = $messageId; 
	 
			// echoing JSON response
			echo json_encode($response);
		}
	}//FINISH
	
} else {
	
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) missing";
	$response["userId"] = $userId; 
	$response["messageId"] = $messageId;
 
    // echoing JSON response
    echo json_encode($response);
}

 

?>
