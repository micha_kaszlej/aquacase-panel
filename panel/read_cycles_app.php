<?php
 
require_once("functions.php");

// connecting to db
$db = new DB_CONNECT();

getWorkCycles();

function getWorkCycles(){
	 
	// check for required fields
	if (isset( $_POST['userId']) && isset( $_POST['userCode']) ) {
	  
		$userId = $_POST['userId'];
		$userCode = $_POST['userCode'];
			
		if (checkUserAuthCode($userId, $userCode))
		{
			
			//TODO CHECK messageId
			$result = mysql_query("SELECT * FROM aquacase_workcycle WHERE aquarium_id = '$userId'");

			// check for empty result
			if (mysql_num_rows($result) > 20) {
				
				$response["workcycles"] = array();
			 
				while ($row = mysql_fetch_array($result)) {
					// temp user array
					$workcycle = array();
					$workcycle["aquariumId"] = $row["aquarium_id"];
					$workcycle["type"] = $row["type"];
					$workcycle["idInType"] = $row["id_in_type"];
					$workcycle["hStart"] = $row["h_start"];
					$workcycle["mStart"] = $row["m_start"];
					$workcycle["hEnd"] = $row["h_end"];
					$workcycle["mEnd"] = $row["m_end"];
					$workcycle["isSet"] = $row["is_set"];
					$workcycle["isEdited"] = $row["is_edited"];
			 
					array_push($response["workcycles"], $workcycle);
				}
				// success
				$response["success"] = 1;
			 
				// echoing JSON response
				echo json_encode($response);
			} 
			else 
			{
				//NO WORKCYCLES FOUND - FILL WITH DEFAULT VALUES
				fillDefaultWorkCycles($userId);
				//SEARCH AGAIN
				getWorkCycles();
			}
		}
	}
	else 
	{
			$response["success"] = 0;
			$response["message"] = "Required field(s) missing";
	 
			// echo no users JSON
			echo json_encode($response);
	}
	
	
}


?>
