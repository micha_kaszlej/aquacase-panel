<?php

require_once("functions.php");
  
// check for required fields
if (isset($_GET['userId']) && isset($_GET['messageId']) && isset($_GET['userCode'])) {
 
	$userId = $_GET['userId'];
	$messageId = $_GET['messageId'];
	$userCode = $_GET['userCode'];
  
    // connecting to db
    $db = new DB_CONNECT() or die("Cannot connect to db");
	
	checkUserAuthCode($userId, $userCode,1) or die("\n[ERROR] Cannot check UserCode");
	checkMessageId($userId, $messageId,1) or die("\n[ERROR] MessageId check failed");
	
	
} else {
	echo "Required field(s) missing";
}

 

?>
