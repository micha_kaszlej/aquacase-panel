<?php
 
require_once("functions.php");

// check for required fields
if (isset($_POST['userId']) && isset($_POST['userCode'])) 
{
  
	$userId = $_POST['userId'];
	$userCode = $_POST['userCode'];
	
    // connecting to db
    $db = new DB_CONNECT();
	 
	if( checkUserAuthCode($userId, $userCode) )
	{
		$result = mysql_query("SELECT * FROM aquacase_status WHERE userId = $userId AND userCode = $userCode ORDER BY messageId DESC LIMIT 1");
	 
		if (!empty($result)) 
		{

			// check for empty result
			if (mysql_num_rows($result) > 0) {
	 
				$result = mysql_fetch_array($result);
	 
				$status = array();
				$status["messageId"] = $result["messageId"];
	 
				// success
				$response["success"] = 1;
	 
				// user node
				$response["status"] = array();
	 
				array_push($response["status"], $status);
	 
				// echoing JSON response
				echo json_encode($response);
				
			} else {
				// no status found
				$response["success"] = 0;
				$response["message"] = "No messages yet";
	 
				echo json_encode($response);
			}
		} 
		else 
		{
			// no status found
			$response["success"] = 0;
			$response["message"] = "No messages yet";
	 
			// echo no users JSON
			echo json_encode($response);
		}
	}
}
else 
{
        $response["success"] = 0;
        $response["message"] = "Requred field(s) missing";
 
        // echo no users JSON
        echo json_encode($response);
}

?>
