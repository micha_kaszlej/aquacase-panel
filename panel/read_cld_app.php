<?php
 
require_once("functions.php");

// check for required fields
if ( isset($_POST['userId']) && isset($_POST['userCode']) ) 
{
	$userId = $_POST['userId'];
	$userCode = $_POST['userCode'];
	
    // connecting to db
    $db = new DB_CONNECT();
	 
	if( checkUserAuthCode($userId, $userCode) )
	{
		$result = mysql_query("SELECT * FROM aquacase_cld WHERE aquariumId = $userId AND userCode = $userCode ORDER BY timestamp");
	 
		if (!empty($result)) 
		{
			// check for empty result
			if (mysql_num_rows($result) > 0) 
			{
				$result = mysql_fetch_array($result);
	 
				$response["calendar_events"] = array();
			 
				while ($row = mysql_fetch_array($result)) {
					$cldTask = array();
					$cldTask["num"] = $row["num"];
					$cldTask["taskId"] = $row["taskId"];
					$cldTask["deleteTask"] = $row["deleteTask"];
					$cldTask["isAquarium"] = $row["isAquarium"];
					$cldTask["aquariumId"] = $row["aquariumId"];
					$cldTask["userId"] = $row["userId"];
					$cldTask["userCode"] = $row["userCode"];
					$cldTask["aquaAck"] = $row["aquaAck"];
					$cldTask["mobileAck"] = $row["mobileAck"];
					$cldTask["date"] = $row["date"];
					$cldTask["time"] = $row["time"];
					$cldTask["alarm"] = $row["alarm"];
					$cldTask["txt"] = $row["txt"];
					
					array_push($response["calendar_events"], $cldTask);
				}
				
				// success
				$response["success"] = 1;
				echo json_encode($response);
				
			} else 
			{
				// no status found
				$response["success"] = 0;
				$response["message"] = "No calendar tasks found";
				echo json_encode($response);
			}
		} 
		else 
		{
			// no status found
			$response["success"] = 0;
			$response["message"] = "No calendar tasks found";
	 
			// echo no users JSON
			echo json_encode($response);
		}
	}
}
else 
{
        $response["success"] = 0;
        $response["message"] = "CldTask: Requred field(s) missing";
 
        // echo no users JSON
        echo json_encode($response);
}

?>
