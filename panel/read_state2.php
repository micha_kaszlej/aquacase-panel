<?php
 
require_once("functions.php");

// check for required fields
if (isset($_POST['userId']) && isset($_POST['userCode'])) 
{
	$userId = $_POST['userId'];
	$userCode = $_POST['userCode'];
	$aquariumId = $_POST['aquariumId'];
	
    // connecting to db
    $db = new DB_CONNECT();
	 
	if( checkUserAuthCode($userId, $userCode) )
	{
		
		if( isset($_POST['newPanel']) )
		{
			$resultNewPanel = mysql_query("UPDATE aquacase_cld set aquaAck = '0' WHERE aquariumId = $aquariumId AND userCode = $userCode ORDER BY timestamp DESC");
		}
	
		if( isset($_POST['cldTaskDelateAckId']) )  // do polaczenia z ack
		{
			$cldTaskDelateAckNum = $_POST['cldTaskDelateAckNum'];
			$cldTaskDelateAckId = $_POST['cldTaskDelateAckId'];
			$resultDel = mysql_query("UPDATE aquacase_cld set aquaAck = '1' WHERE aquariumId = $aquariumId AND userCode = $userCode AND aquaAck = 0 AND num = $cldTaskDelateAckNum ORDER BY timestamp DESC LIMIT 1");
		}
		
		if( isset($_POST['ackCldTaskNum']) && isset($_POST['ackCldTaskId']) )
		{
			$ackCldTaskNum = $_POST['ackCldTaskNum'];
			$ackCldTaskId = $_POST['ackCldTaskId'];
			$resultAck = mysql_query("UPDATE aquacase_cld set aquaAck = '1', taskId = '$ackCldTaskId' WHERE aquariumId = $aquariumId AND userCode = $userCode AND aquaAck = 0 AND num = $ackCldTaskNum ORDER BY timestamp DESC LIMIT 1");
		}
		$resultCld = mysql_query("SELECT num FROM aquacase_cld WHERE aquariumId = $aquariumId AND userCode = $userCode AND aquaAck = 0 ORDER BY timestamp DESC LIMIT 1");
		$result = mysql_query("SELECT * FROM aquacase_status WHERE userId = $userId AND userCode = $userCode ORDER BY messageId DESC LIMIT 1");// ORDER BY currentTime DESC -> ORDER BY messageId DESC
	 
		if (!empty($result)) 
		{
			// check for empty result
			if (mysql_num_rows($result) > 0) 
			{
				$result = mysql_fetch_array($result);
	 
				$status = array();
				$status["userId"] = $result["userId"];
				$status["messageId"] = $result["messageId"];
				$status["userCode"] = $result["userCode"];
				$status["isAquarium"] = $result["isAquarium"];
				$status["currentTime"] = $result["currentTime"];
				$status["mode"] = $result["mode"];
				$status["currentScreenId"] = $result["currentScreenId"];
				$status["currentScreenType"]= $result["currentScreenType"];
				$status["pinCode"] = $result["pinCode"];
				$status["leftPumpActive"] = $result["leftPumpActive"];
				$status["rightPumpActive"] = $result["rightPumpActive"];
				$status["lightActive"] = $result["lightActive"];
				$status["rgbActive"] = $result["rgbActive"];
				$status["outRgbActive"] = $result["outRgbActive"];
				$status["o2Active"] = $result["o2Active"];
				$status["volcanoActive"] = $result["volcanoActive"];
				$status["co2Active"] = $result["co2Active"];
				$status["foodActive"] = $result["foodActive"];
				$status["lightPower"] = $result["lightPower"];
				$status["currentTemperature"] = $result["currentTemperature"];
				$status["desiredTemperature"] = $result["desiredTemperature"];
				$status["rgbAnimationType"] = $result["rgbAnimationType"];
				$status["rgbPower"] = $result["rgbPower"];
				$status["outRgbAnimationType"] = $result["outRgbAnimationType"];
				$status["outRgbPower"] = $result["outRgbPower"];
				$status["rgbColor"] = $result["rgbColor"];
				$status["outRgbColor"] = $result["outRgbColor"];
				$status["languageCode"] = $result["languageCode"];
				$status["bgResourceId"] = $result["bgResourceId"];
				$status["dispTimeOff"] = $result["dispTimeOff"];
				$status["dispTimeLock"] = $result["dispTimeLock"];
				$status["rgbColorNo"] = $result["rgbColorNo"];
				$status["outRgbColorNo"] = $result["outRgbColorNo"];
				
				
				if (!empty($resultCld)) 
				{
					if (mysql_num_rows($resultCld) > 0) 
					{
						$resultCld = mysql_fetch_array($resultCld);
						$status["syncTaskNum"] = $resultCld["num"];
					}
				}
				
				if (!empty($resultDel)) 
				{
					$status["delTaskAckNum"] = $cldTaskDelateAckNum;
				}
				
				if( !empty($resultAck) )
				{
					$status["taskAckNum"] = $ackCldTaskNum;
				}
				
				
				if( isset($_POST['delCldTaskId']) )
				{
					$delCldTaskId = $_POST['delCldTaskId'];
					$resultDelCldTask = mysql_query("UPDATE aquacase_cld set deleteTask = '1' WHERE aquariumId = $aquariumId AND userCode = $userCode AND taskId = $delCldTaskId ORDER BY timestamp DESC LIMIT 1");
					$status["taskDelated"] = $delCldTaskId;
				}
				
	 
				// success
				$response["success"] = 1;
	 
				// user node
				$response["status"] = array();
	 
				array_push($response["status"], $status);
	 
				// echoing JSON response
				echo str_replace( '"','', json_encode($response) );
				
			} else {
				// no status found
				$response["success"] = 0;
				$response["message"] = "No status found";
	 
				echo json_encode($response);
			}
		} 
		else 
		{
			// no status found
			$response["success"] = 0;
			$response["message"] = "No status found";
	 
			// echo no users JSON
			echo json_encode($response);
		}
	}
}
else 
{
        $response["success"] = 0;
        $response["message"] = "Requred field(s) missing";
 
        // echo no users JSON
        echo json_encode($response);
}

?>
