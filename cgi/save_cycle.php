<?php
 
require_once("functions.php");

// check for required fields
if ( isset($_POST['aquariumId']) &&  isset($_POST['userCode']) ) {

	$userId = $_POST['aquariumId'];
	$userCode = $_POST['userCode'];
	$type = $_POST['type'];
	$idInType = $_POST['idInType'];
	$hStart = $_POST['hStart'];
	$mStart = $_POST['mStart'];
	$hEnd = $_POST['hEnd'];
	$mEnd = $_POST['mEnd'];
	$isSet = $_POST['isSet'];
	$isEdited = $_POST['isEdited'];
 
 
    // connecting to db
    $db = new DB_CONNECT();

	if (checkUserAuthCode($userId, $userCode))
	{
		hasDefinedCycles($userId);
		
		 // mysql inserting a new row
		$result = mysql_query("INSERT INTO aquacase_workcycle(aquarium_id, type, id_in_type, h_start, m_start, h_end, m_end, is_set, is_edited)
		VALUES('$aquariumId', '$type', '$idInType', '$hStart', '$mStart', '$hEnd', '$mEnd', '$isSet','$isEdited') 
		ON DUPLICATE KEY UPDATE aquarium_id='$aquariumId', type='$type', id_in_type='$idInType', h_start='$hStart', m_start='$mStart', h_end='$hEnd', m_end='$mEnd', is_set='$isSet', is_edited='$isEdited';");
	 
		// check if row inserted or not
		if ($result) {
			// successfully inserted into database
			$response["success"] = 1;
			$response["message"] = "Work Cycle Row successfully created/updated.";
	 
			echo json_encode($response);
		} else {
			// failed to insert row
			$response["success"] = 0;
			$response["message"] = "Work Cycle update failed";
	 
			echo json_encode($response);
		}
	}//FINISH
	
} else 
{
	
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    echo json_encode($response);
}

?>
